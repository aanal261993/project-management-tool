﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="leftmenu.ascx.cs" Inherits="PMToolWeb.controls.leftmenu" %>

<div class="sidebar_search">
            <form>
            <input type="text" name="" class="search_input" value="search keyword" onclick="this.value=''" />
            <input type="image" class="search_submit" src="<%= PMToolWeb.AppSetting.GetSetting("ApplicationURL", PMToolWeb.AppSettingCategory.General) %>/images/search.png" />
            </form>            
            </div>

    
            <div class="sidebarmenu" >
            
     
            <div class="sidebarmenu"  runat="server" id="LeftmenuControl" >
              <a class="menuitem submenuheader" href="" id="Account" runat="server">Account</a>
                <div class="submenu">
                    <ul>
                    <li><a href="~/editprofiledetail.aspx" id="edituserprofile" runat="server"  >Edit Profile</a></li>
                    <li><a href="~/UpdatePassword.aspx" id="ChangePassword" runat="server">Change Password</a></li>
                   
                    </ul>
                </div>
                
               
    
    <a  class="menuitem" id="Role" runat="server" href="~/role/role.aspx"><span>Role Management</span></a>
    <a class="menuitem" id="Activity" runat="server" href="~/role/activity.aspx"><span>Activity Management</span></a>
    <a class="menuitem" id="ProjectQueue" runat="server" href='~/project-queue.aspx'><span>Active Projects</span></a>
    <a class="menuitem_red" id="ProjectCreator" runat="server" href='~/project-creator.aspx'><span>Create New Project</span></a>
    <a class="menuitem" id="ProjectTaskQueue" runat="server" href='~/task-list.aspx'><span>All Project Tasks</span></a>
    <a class="menuitem" id="CreateNewTask" runat="server" href='~/create-task.aspx'><span>Create New Task</span></a>
    <a class="menuitem" id="MyPrivateTasks" runat="server" href='~/my-private-task.aspx'><span>My Private Tasks</span></a>
    <a class="menuitem" id="UserManager" runat="server" href='~/user-management.aspx'><span>User Management</span></a>
    <a class="menuitem" id="Reopen" runat="server" href='~/OpenTaskList.aspx'><span>Reopen</span></a>
     <a class="menuitem" id="Review" runat="server" href='~/ReviewByclient.aspx'><span>Completed Task</span></a>
    <a class="menuitem" id="Closedtasks" runat="server" href='~/closed-tasks.aspx'><span>Closed Tasks</span></a>
    <a class="menuitem_red" id="Company" runat="server" href='~/Company.aspx'><span>Company</span></a>
    <a class="menuitem submenuheader" href="" id="ReportDetails" runat="server">Report</a>
                <div class="submenu">
                    <ul>
                    <li><a href="~/ClientReport.aspx" id="A2" runat="server"  >Cilent Report</a></li>
                    <li><a href="~/MyReport.aspx" id="A3" runat="server" > My Report</a></li>
                    <li><a href="~/employeeReport.aspx" id="A4" runat="server">Employee Report</a></li>
                    </ul>
                </div>
    <a class="menuitem" id="SignOut" runat="server" href='~/account/signout.aspx'><span>SignOut</span></a>

               
                    
            </div>
            
            
            <div class="sidebar_box">
                <div class="sidebar_box_top"></div>
                <div class="sidebar_box_content">
                <h3>User help desk</h3>
                <img src="images/info.png" alt="" title="" class="sidebar_icon_right" />
                <p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>                
                </div>
                <div class="sidebar_box_bottom"></div>
            </div>
            
            <div class="sidebar_box">
                <div class="sidebar_box_top"></div>
                <div class="sidebar_box_content">
                <h4>Important notice</h4>
                <img src="images/notice.png" alt="" title="" class="sidebar_icon_right" />
                <p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>                
                </div>
                <div class="sidebar_box_bottom"></div>
            </div>
            
            <div class="sidebar_box">
                <div class="sidebar_box_top"></div>
                <div class="sidebar_box_content">
                <h5>Download photos</h5>
                <img src="images/photo.png" alt="" title="" class="sidebar_icon_right" />
                <p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>                
                </div>
                <div class="sidebar_box_bottom"></div>
            </div>  
            
            <div class="sidebar_box">
                <div class="sidebar_box_top"></div>
                <div class="sidebar_box_content">
                <h3>To do List</h3>
                <img src="images/info.png" alt="" title="" class="sidebar_icon_right" />
                <ul>
                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                 <li>Lorem ipsum dolor sit ametconsectetur <strong>adipisicing</strong> elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                  <li>Lorem ipsum dolor sit amet, consectetur <a href="#">adipisicing</a> elit.</li>
                   <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                     <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                </ul>                
                </div>
                <div class="sidebar_box_bottom"></div>
            </div>

</div>
</form>