﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/default.Master" AutoEventWireup="true" CodeBehind="MyReport.aspx.cs" Inherits="PMToolWeb.MyReport" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v12.2.Web, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 55px;
        }
    </style>
    <script>
        function printImage(image) {
            var printWindow = window.open('', 'Print Window', 'height=400,width=600');
            printWindow.document.write('<html><head><title>Print Window</title>');
            printWindow.document.write('</head><body ><img src=\'');
            printWindow.document.write(image.src);
            printWindow.document.write('\' /></body></html>');
            printWindow.document.close();
            printWindow.print();
        }
        function prin() {
            var image = document.getElementById('ctl00_cphAdmin_WebChartControl1_IMG');
            printImage(image);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">

    <br />

    <table class="auto-style1">
      
        <tr>
            <td>Select Report Type</td>
            <td>
                <dx:ASPxComboBox ID="ddlreporttype" runat="server">
                    <Items>
                        <dx:ListEditItem Text="Weekly" Value="0" />
                        <dx:ListEditItem Text="Monthly" Value="1" />
                    </Items>
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <dx:ASPxButton ID="btnshowreport" runat="server" OnClick="btnshowreport_Click" Text="Show Report" Width="153px">
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <dxchartsui:WebChartControl ID="WebChartControl1" runat="server"  Height="356px" Width="491px">
                   

                </dxchartsui:WebChartControl>
              
            </td>
        </tr>
    </table>
    <center>
        <input id="Button1" type="button" value="Print" onclick="prin()" /></center>
</asp:Content>
