﻿<%@ Page Title="Proman | Task Detail" Language="C#" MasterPageFile="~/masterpages/default.Master"
    AutoEventWireup="true" CodeBehind="task-detail.aspx.cs" Inherits="PMToolWeb.task_detail" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Linear" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Circular" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.State" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Digital" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../scripts/jquery.js"></script>
    <script type="text/javascript" src="../scripts/tipsy.js"></script>
    <script type="text/javascript" src="scripts/audio-player.js"></script>
    <script type="text/javascript">
        AudioPlayer.setup("scripts/player.swf", {
            width: 258,
            initialvolume: 100,
            transparentpagebg: "yes",
            left: "000000",
            lefticon: "FFFFFF"
        });

        function SetMaxLength(memo, maxLength) {
            if (!memo)
                return;
            if (typeof (maxLength) != "undefined" && maxLength >= 0) {
                memo.maxLength = maxLength;
                memo.maxLengthTimerToken = window.setInterval(function () {
                    var text = memo.GetText();
                    if (text && text.length > memo.maxLength)
                        memo.SetText(text.substr(0, memo.maxLength));
                }, 10);
            } else if (memo.maxLengthTimerToken) {
                window.clearInterval(memo.maxLengthTimerToken);
                delete memo.maxLengthTimerToken;
                delete memo.maxLength;
            }
        }
        function OnMaxLengthChanged(s, e) {
            var maxLength = 750;
            SetMaxLength(memo, maxLength);
        }


        $(document).ready(function () {
            $(".toolTip").mouseover(function (e) {
                $(this).tipsy({ fade: true, gravity: 'sw' });

            });
        });

        function OnProjectIdChanged(cmbProject) {
            ddlClientTask.PerformCallback(cmbProject.GetValue().toString());
        }
        function OnDateChanged(dtDate) {

            gvClientTimeEntry.PerformCallback(dtDate.GetText().toString());

        }
     

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
    <div class="task-detail">
        <div class="heading">
            <h2>
                Task Details
            </h2>
        </div>
        <div class="maindiv">
            <div class="subdiv">
                <div class="div-message default-padding-left">
                    <asp:Label ID="lblStatus" runat="server" EnableViewState="false"></asp:Label>
                </div>
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="200px">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent1" runat="server">
                            <dx:ASPxPopupControl ID="pcTimeEntry" runat="server" HeaderText="Time Entry" PopupHorizontalAlign="WindowCenter"
                                PopupVerticalAlign="Above" Modal="True" CloseAction="CloseButton" PopupVerticalOffset="25">
                                <ContentCollection>
                                    <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
                                        <center>
                                            <asp:Label ID="lblTimeEntryStatus" runat="server" Text="">
                                            </asp:Label>
                                            <table width="400px">
                                                <tr>
                                                    <td class="pclefttd">
                                                        Project:
                                                    </td>
                                                    <td colspan="5">
                                                        &nbsp;&nbsp;<dx:ASPxComboBox ID="ddlProject" IncrementalFilteringMode="StartsWith"
                                                            runat="server" ValueType="System.String" Width="300px">
                                                            <ValidationSettings Display="None">
                                                            </ValidationSettings>
                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) {OnProjectIdChanged(s); }" />
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pclefttd">
                                                        Task:
                                                    </td>
                                                    <td colspan="5">
                                                        <dx:ASPxComboBox ID="ddlTask" IncrementalFilteringMode="StartsWith" ClientInstanceName="ddlClientTask"
                                                            runat="server" ValueType="System.String" Width="300px" OnCallback="CmbTask_Callback">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True">
                                                                <RequiredField IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pclefttd">
                                                        EntryDate:
                                                    </td>
                                                    <td colspan="5">
                                                        <dx:ASPxDateEdit ID="dtTimeEntry" runat="server" Width="130px">
                                                            <ClientSideEvents DateChanged="function(s, e) {OnDateChanged(s); }" />
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True">
                                                            </ValidationSettings>
                                                        </dx:ASPxDateEdit>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pclefttd">
                                                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" RightToLeft="False" Text="From Time:"
                                                            Width="70px">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td class="pclefttd">
                                                        <dx:ASPxComboBox ID="ddlFromHrs" runat="server" SelectedIndex="0" ValueType="System.String"
                                                            Width="50px">
                                                            <Items>
                                                                <dx:ListEditItem Selected="True" Text="00" Value="00" />
                                                                <dx:ListEditItem Text="01" Value="01" />
                                                                <dx:ListEditItem Text="02" Value="02" />
                                                                <dx:ListEditItem Text="03" Value="03" />
                                                                <dx:ListEditItem Text="04" Value="04" />
                                                                <dx:ListEditItem Text="05" Value="05" />
                                                                <dx:ListEditItem Text="06" Value="06" />
                                                                <dx:ListEditItem Text="07" Value="07" />
                                                                <dx:ListEditItem Text="08" Value="08" />
                                                                <dx:ListEditItem Text="09" Value="09" />
                                                                <dx:ListEditItem Text="10" Value="10" />
                                                                <dx:ListEditItem Text="11" Value="11" />
                                                                <dx:ListEditItem Text="12" Value="12" />
                                                                <dx:ListEditItem Text="13" Value="13" />
                                                                <dx:ListEditItem Text="14" Value="14" />
                                                                <dx:ListEditItem Text="15" Value="15" />
                                                                <dx:ListEditItem Text="16" Value="16" />
                                                                <dx:ListEditItem Text="17" Value="17" />
                                                                <dx:ListEditItem Text="18" Value="18" />
                                                                <dx:ListEditItem Text="19" Value="19" />
                                                                <dx:ListEditItem Text="20" Value="20" />
                                                                <dx:ListEditItem Text="21" Value="21" />
                                                                <dx:ListEditItem Text="22" Value="22" />
                                                                <dx:ListEditItem Text="23" Value="23" />
                                                                <dx:ListEditItem Text="24" Value="24" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxComboBox ID="ddlFromMinites" runat="server" SelectedIndex="0" ValueType="System.String"
                                                            Width="50px">
                                                            <Items>
                                                                <dx:ListEditItem Selected="True" Text="00" Value="0" />
                                                                <dx:ListEditItem Text="15" Value="15" />
                                                                <dx:ListEditItem Text="30" Value="30" />
                                                                <dx:ListEditItem Text="45" Value="45" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                    <td>
                                                        To Time:
                                                    </td>
                                                    <td>
                                                        <dx:ASPxComboBox ID="ddlToHrs" runat="server" SelectedIndex="0" ValueType="System.String"
                                                            Width="50px">
                                                            <Items>
                                                                <dx:ListEditItem Selected="True" Text="00" Value="00" />
                                                                <dx:ListEditItem Text="01" Value="01" />
                                                                <dx:ListEditItem Text="02" Value="02" />
                                                                <dx:ListEditItem Text="03" Value="03" />
                                                                <dx:ListEditItem Text="04" Value="04" />
                                                                <dx:ListEditItem Text="05" Value="05" />
                                                                <dx:ListEditItem Text="06" Value="06" />
                                                                <dx:ListEditItem Text="07" Value="07" />
                                                                <dx:ListEditItem Text="08" Value="08" />
                                                                <dx:ListEditItem Text="09" Value="09" />
                                                                <dx:ListEditItem Text="10" Value="10" />
                                                                <dx:ListEditItem Text="11" Value="11" />
                                                                <dx:ListEditItem Text="12" Value="12" />
                                                                <dx:ListEditItem Text="13" Value="13" />
                                                                <dx:ListEditItem Text="14" Value="14" />
                                                                <dx:ListEditItem Text="15" Value="15" />
                                                                <dx:ListEditItem Text="16" Value="16" />
                                                                <dx:ListEditItem Text="17" Value="17" />
                                                                <dx:ListEditItem Text="18" Value="18" />
                                                                <dx:ListEditItem Text="19" Value="19" />
                                                                <dx:ListEditItem Text="20" Value="20" />
                                                                <dx:ListEditItem Text="21" Value="21" />
                                                                <dx:ListEditItem Text="22" Value="22" />
                                                                <dx:ListEditItem Text="23" Value="23" />
                                                                <dx:ListEditItem Text="24" Value="24" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxComboBox ID="ddlToMinites" runat="server" SelectedIndex="0" ValueType="System.String"
                                                            Width="50px">
                                                            <Items>
                                                                <dx:ListEditItem Selected="True" Text="00" Value="00" />
                                                                <dx:ListEditItem Text="15" Value="15" />
                                                                <dx:ListEditItem Text="30" Value="30" />
                                                                <dx:ListEditItem Text="45" Value="45" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="addresslefttd">
                                                        Comment:
                                                    </td>
                                                    <td colspan="5">
                                                        <dx:ASPxMemo ID="txtCommentTime" runat="server" Height="71px" Width="300px">
                                                        </dx:ASPxMemo>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pclefttd">
                                                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" RightToLeft="False" Text="Task Status:"
                                                            Width="80px">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td colspan="5">
                                                        <dx:ASPxComboBox ID="ddlTaskStatusPc" runat="server" SelectedIndex="0" ValueType="System.String"
                                                            Width="130px">
                                                            <Items>
                                                                <dx:ListEditItem Selected="True" Text="Select Status" Value="0" />
                                                                <dx:ListEditItem Text="Open" Value="1" />
                                                                <dx:ListEditItem Text="InProgress" Value="2" />
                                                                <dx:ListEditItem Text="Pending" Value="3" />
                                                                <dx:ListEditItem Text="Closed" Value="4" />
                                                            </Items>
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                                            </ValidationSettings>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                    </td>
                                                    <td colspan="2">
                                                        <br />
                                                        <dx:ASPxButton ID="btnTimeEntrySave" runat="server" OnClick="btnTimeEntrySave_Click"
                                                            Text="Save">
                                                        </dx:ASPxButton>
                                                        <br />
                                                    </td>
                                                    <td colspan="2">
                                                        <br />
                                                        <dx:ASPxButton ID="btnCancel" runat="server" OnClick="btnTimeEntryCancle_Click" Text="Cancel">
                                                        </dx:ASPxButton>
                                                        <br />
                                                    </td>
                                                    <td colspan="2">
                                                    </td>
                                                </tr>
                                            </table>
                                        </center>
                                        <center>
                                            <dx:ASPxGridView ID="gvTaskTime" runat="server" AutoGenerateColumns="False" KeyFieldName="TaskTimeId"
                                                OnRowCommand="gvTaskTime_RowCommand" OnCustomCallback="dtTimeEntry_DateChanged"
                                                ClientInstanceName="gvClientTimeEntry" OnHtmlRowCreated="gvTaskTime_HtmlRowCreated"
                                                Width="800">
                                                <Columns>
                                                    <dx:GridViewDataColumn Caption="TaskID" FieldName="TaskId" Width="20" />
                                                    <dx:GridViewDataColumn Caption="ProjectName" FieldName="ProjectName" Width="150" />
                                                    <dx:GridViewDataColumn Caption="TaskTitle" FieldName="TaskName" />
                                                    <dx:GridViewDataColumn Caption="EntryDate" FieldName="TimeEntryDate" Width="25" />
                                                    <dx:GridViewDataColumn Caption="FromTime" FieldName="FromTime" Width="80" />
                                                    <dx:GridViewDataColumn Caption="ToTime" FieldName="ToTime" Width="80" />
                                                    <dx:GridViewDataColumn Caption="Comment" FieldName="Comment" />
                                                    <dx:GridViewDataColumn Caption="Edit">
                                                        <DataItemTemplate>
                                                            <asp:LinkButton ForeColor="Blue" Font-Underline="true" CommandName="Edit" CommandArgument='<%# Eval("TaskTimeId") %>'
                                                                ID="imgbtnEdit" runat="server" Text="Edit" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Caption="Delete">
                                                        <DataItemTemplate>
                                                            <asp:LinkButton ForeColor="Blue" Font-Underline="true" CommandName="Delete" OnClientClick="javascript:return confirm('Are you sure want to delete this Entry?');"
                                                                CommandArgument='<%# Eval("TaskTimeId") %>' ID="lnkbtnDelete" runat="server"
                                                                Text="Delete" Width="20" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                                            </dx:ASPxGridView>
                                        </center>
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                            </dx:ASPxPopupControl>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
                <br />
                <table cellpadding="2" cellspacing="3" border="0">
                    <tr>
                        <td>
                            <strong>
                                <dx:ASPxLabel ID="lblCompanyName" runat="server">
                                </dx:ASPxLabel>
                            </strong>
                        </td>
                        <td>
                            <strong>
                                <dx:ASPxLabel ID="lblProjectName" runat="server">
                                </dx:ASPxLabel>
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="25%" valign="top">
                            <dx:ASPxImage ID="imgProject" runat="server" Width="170px" Height="115px">
                            </dx:ASPxImage>
                        </td>
                        <td align="left" valign="top">
                            <table cellpadding="2" cellspacing="1" border="0">
                                <tr>
                                    <td align="left" valign="top">
                                        <table cellpadding="2" cellspacing="1" border="0">
                                            <tr>
                                                <td align="right" valign="top">
                                                    Project Number:
                                                </td>
                                                <td>
                                                    <strong>
                                                        <dx:ASPxLabel ID="lblProjectNo" runat="server" Text="ASPxLabel">
                                                        </dx:ASPxLabel>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top">
                                                    Project Status:
                                                </td>
                                                <td>
                                                    <strong>
                                                        <dx:ASPxLabel ID="lblProjectStatus" runat="server" Text="ASPxLabel">
                                                        </dx:ASPxLabel>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top">
                                                    Days Project Open:
                                                </td>
                                                <td>
                                                    <strong>
                                                        <dx:ASPxLabel ID="lblDaysProjectOpen" runat="server" Text="ASPxLabel">
                                                        </dx:ASPxLabel>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top">
                                                    Days Remaining to Complete:
                                                </td>
                                                <td>
                                                    <strong>
                                                        <dx:ASPxLabel ID="lblDaysRemainingToComplete" runat="server" Text="ASPxLabel">
                                                        </dx:ASPxLabel>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top">
                                                    Project Owner:
                                                </td>
                                                <td>
                                                    <strong>
                                                        <dx:ASPxLabel ID="lblProjectOwner" runat="server" Text="ASPxLabel">
                                                        </dx:ASPxLabel>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top">
                                                    Open Tasks:
                                                </td>
                                                <td>
                                                    <strong>
                                                        <dx:ASPxLabel ID="lblOpenTasks" runat="server" Text="ASPxLabel">
                                                        </dx:ASPxLabel>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top">
                                                    Project Completion Status:
                                                </td>
                                                <td>
                                                    <strong>
                                                        <dx:ASPxLabel ID="lblProjectCompletionStatus" runat="server" Text="ASPxLabel">
                                                        </dx:ASPxLabel>
                                                        % </strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="center" valign="top">
                                        <asp:Panel ID="pnlGuage" runat="server" Height="135px" ScrollBars="None" Style="overflow: hidden;">
                                            <dx:ASPxGaugeControl ID="gaugeProjectCompletionStatus" runat="server" BackColor="White"
                                                ClientIDMode="AutoID" Height="170px" Value="40" Width="170px">
                                            </dx:ASPxGaugeControl>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="2">
                            <div style="margin-top: -20px;">
                                <div class="project-desc-title">
                                    <strong>Project Description:</strong>
                                </div>
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" valign="top" class="project-description">
                                            <div>
                                                <dx:ASPxLabel ID="lblProjectDescription" runat="server" Text="ASPxLabel" Width="165px">
                                                </dx:ASPxLabel>
                                            </div>
                                        </td>
                                        <td align="left" valign="top" width="5px">
                                            &nbsp;
                                        </td>
                                        <td align="left" valign="top">
                                            <table style="" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <div class="sub-heading">
                                                            <h2>
                                                                Project Tasks
                                                            </h2>
                                                            <dx:ASPxComboBox CssClass="ddlTaskSelectByCategory" Width="100px" ID="ddlTaskStatus"
                                                                runat="server" ValueType="System.String" AutoPostBack="True" OnSelectedIndexChanged="ddlTaskStatus_SelectedIndexChanged">
                                                                <Items>
                                                                    <dx:ListEditItem Text="--All--" Value="0" />
                                                                    <dx:ListEditItem Text="Open" Value="1" />
                                                                    <dx:ListEditItem Text="InProgress" Value="2" />
                                                                    <dx:ListEditItem Text="Pending" Value="3" />
                                                                    <dx:ListEditItem Text="Closed" Value="4" />
                                                                </Items>
                                                            </dx:ASPxComboBox>
                                                            <dx:ASPxButton ID="btnTimeEntry" CssClass="btnTimeEntry" runat="server" Text="Time Entry"
                                                                OnClick="btnTimeEntry_Click">
                                                            </dx:ASPxButton>
                                                        </div>
                                                        <dx:ASPxGridView ID="gvProjectTask" ClientInstanceName="gvProjectTask" runat="server"
                                                            KeyFieldName="TaskId" AutoGenerateColumns="False" Width="595px" OnRowCommand="gvProjectTask_RowCommand">
                                                            <Columns>
                                                                <dx:GridViewDataColumn Caption="View" VisibleIndex="0" Width="25px">
                                                                    <DataItemTemplate>
                                                                        <asp:LinkButton ID="lnkbtnTaskId" runat="server" CommandName="View" CausesValidation="false"
                                                                            CommandArgument='<%# Eval("TaskId") %>'>
                                                                            <img id="imgView" runat="server" src="~/images/view.png" alt="View" style="border: 0px;"
                                                                                title="View" /></asp:LinkButton>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="Assignee Name" FieldName="AssignToName" Width="110px" />
                                                                <dx:GridViewDataColumn Caption="Task Num." FieldName="TaskId" Width="25px" />
                                                                <dx:GridViewDataColumn Caption="WF ID" FieldName="WireframeNo" Width="25px" />
                                                                <dx:GridViewDataColumn Caption="Task Title" FieldName="Title" />
                                                                <dx:GridViewDataColumn Caption="Edit" Width="25px">
                                                                    <DataItemTemplate>
                                                                        <a href='create-task.aspx?taskid=<%# Eval("TaskId") %>' class="project-name">Edit</a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataColumn>
                                                            </Columns>
                                                            <SettingsPager PageSize="5">
                                                            </SettingsPager>
                                                            <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                                                        </dx:ASPxGridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <div class="details" runat="server" id="divDetail">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td align="left" valign="top" class="task-header">
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td align="left" valign="top" width="15%">
                                                                                    Task:&nbsp;
                                                                                    <dx:ASPxLabel ID="lblTaskNo" runat="server">
                                                                                    </dx:ASPxLabel>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <div class="task-title">
                                                                                        <dx:ASPxLabel ID="lblTaskTitle" runat="server">
                                                                                        </dx:ASPxLabel>
                                                                                    </div>
                                                                                </td>
                                                                                <td align="right" valign="top">
                                                                                    Days Remaining:&nbsp;<dx:ASPxLabel ID="lblDaysRemaining" runat="server">
                                                                                    </dx:ASPxLabel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <div class="task-status-row">
                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                <tr>
                                                                                    <td align="left" valign="top" colspan="3" class="task-completion-label" style="font-size: 12px;">
                                                                                        Open Time:&nbsp;<asp:Label ID="lblOpenTime" runat="server" Text="Label"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" colspan="3" class="task-completion-label" style="font-size: 12px;
                                                                                        border-bottom: solid 1px gray;">
                                                                                        Number of Updates:&nbsp;<asp:Label ID="lblTotalUpdates" runat="server" Text="Label"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" width="25%">
                                                                                        <div class="task-completion-label">
                                                                                            Task Completion Status</div>
                                                                                    </td>
                                                                                    <td align="left" valign="top">
                                                                                        <div class="task-completion-status">
                                                                                            <dx:ASPxLabel ID="lblTaskCompletionStatus" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td align="left" valign="middle">
                                                                                        <div class="status-images">
                                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                                <tr>
                                                                                                    <td align="left" valign="middle">
                                                                                                        <asp:ImageButton ID="imgbtnStatus1" CssClass="toolTip" runat="server" ImageUrl="~/images/dotempty.jpg"
                                                                                                            CommandArgument="10" OnClick="imgbtnStatus_Click" Width="24px" Height="24px"
                                                                                                            ToolTip="Brain Storming" />
                                                                                                    </td>
                                                                                                    <td align="left" valign="middle">
                                                                                                        <asp:ImageButton ID="imgbtnStatus2" CssClass="toolTip" runat="server" ImageUrl="~/images/dotempty.jpg"
                                                                                                            CommandArgument="20" OnClick="imgbtnStatus_Click" ToolTip="Discussion" />
                                                                                                    </td>
                                                                                                    <td align="left" valign="middle">
                                                                                                        <asp:ImageButton ID="imgbtnStatus3" CssClass="toolTip" runat="server" ImageUrl="~/images/dotempty.jpg"
                                                                                                            CommandArgument="30" OnClick="imgbtnStatus_Click" ToolTip=" Debate" />
                                                                                                    </td>
                                                                                                    <td align="left" valign="middle">
                                                                                                        <asp:ImageButton ID="imgbtnStatus4" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                                                                            CommandArgument="40" OnClick="imgbtnStatus_Click" ToolTip=" Scoping" />
                                                                                                    </td>
                                                                                                    <td align="left" valign="middle">
                                                                                                        <asp:ImageButton ID="imgbtnStatus5" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                                                                            CommandArgument="50" OnClick="imgbtnStatus_Click" ToolTip="Programming" />
                                                                                                    </td>
                                                                                                    <td align="left" valign="middle">
                                                                                                        <asp:ImageButton ID="imgbtnStatus6" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                                                                            CommandArgument="60" OnClick="imgbtnStatus_Click" ToolTip=" Testing" />
                                                                                                    </td>
                                                                                                    <td align="left" valign="middle">
                                                                                                        <asp:ImageButton ID="imgbtnStatus7" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                                                                            CommandArgument="70" OnClick="imgbtnStatus_Click" ToolTip="Bug Found" />
                                                                                                    </td>
                                                                                                    <td align="left" valign="middle">
                                                                                                        <asp:ImageButton ID="imgbtnStatus8" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                                                                            CommandArgument="80" OnClick="imgbtnStatus_Click" ToolTip="Bug Fixes" />
                                                                                                    </td>
                                                                                                    <td align="left" valign="middle">
                                                                                                        <asp:ImageButton ID="imgbtnStatus9" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                                                                            CommandArgument="90" OnClick="imgbtnStatus_Click" ToolTip="Awaiting Client Approval" />
                                                                                                    </td>
                                                                                                    <td align="left" valign="middle">
                                                                                                        <asp:ImageButton ID="imgbtnStatus10" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                                                                            CommandArgument="100" OnClick="imgbtnStatus_Click" ToolTip="Closed" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <table cellpadding="2" cellspacing="3" border="0">
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    <img src='<%= PMToolWeb.AppSetting.GetSetting("ApplicationURL", PMToolWeb.AppSettingCategory.General) %>/images/flag.jpg'
                                                                                        alt="flag" />
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <div class="update-button">
                                                                                        <strong>Task Status:</strong>
                                                                                    </div>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <dx:ASPxHiddenField ID="hdnTaskStatus" runat="server">
                                                                                    </dx:ASPxHiddenField>
                                                                                    <dx:ASPxRadioButtonList ID="rdoListTaskStatus" runat="server" RepeatDirection="Horizontal">
                                                                                        <Items>
                                                                                            <dx:ListEditItem Text="Open" Value="1" />
                                                                                            <dx:ListEditItem Text="In Progress" Value="2" />
                                                                                            <dx:ListEditItem Text="Pend" Value="3" />
                                                                                            <dx:ListEditItem Text="Closed" Value="4" />
                                                                                        </Items>
                                                                                    </dx:ASPxRadioButtonList>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <div class="update-button">
                                                                                        <asp:LinkButton ID="lnkbtnUpdateTaskStatus" runat="server" CausesValidation="false"
                                                                                            OnClick="lnkbtnUpdateTaskStatus_Click">Update</asp:LinkButton>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <table cellpadding="2" cellspacing="3" border="0">
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    Due Date:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <strong>
                                                                                        <dx:ASPxLabel ID="lblDueDate" runat="server" Text="">
                                                                                        </dx:ASPxLabel>
                                                                                    </strong>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    Change Due Date:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <dx:ASPxDateEdit ID="dtDueDate" runat="server" Width="85px">
                                                                                    </dx:ASPxDateEdit>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <table cellpadding="2" cellspacing="3" border="0">
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    <strong>Task Owner:</strong>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <strong>
                                                                                        <dx:ASPxLabel ID="lblTaskOwner" runat="server">
                                                                                        </dx:ASPxLabel>
                                                                                    </strong>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    <strong>Task URL:</strong>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <strong>
                                                                                        <dx:ASPxLabel ID="lblTaskURL" runat="server">
                                                                                        </dx:ASPxLabel>
                                                                                    </strong>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" valign="top">
                                                                        <dx:ASPxDataView ID="dvTaskImages" runat="server" RowPerPage="5" ItemStyle-Paddings-Padding="0px"
                                                                            ItemSpacing="10px" EmptyDataText="[No image uploaded]" ColumnCount="2">
                                                                            <ItemTemplate>
                                                                                <div>
                                                                                    <a href='<%#  ResolveClientUrl(PMToolWeb.Helper.GetTaskImage(this.TaskID, Convert.ToString(Eval("ImageName")))) %>'
                                                                                        target="_blank">
                                                                                        <dx:ASPxImage ID="imgTaskImage" runat="server" ImageUrl='<%# PMToolWeb.Helper.GetTaskImage(this.TaskID, Convert.ToString(Eval("ImageName"))) %>'
                                                                                            Width="154px" Height="154px" />
                                                                                    </a>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                            <PagerSettings>
                                                                                <AllButton Visible="True">
                                                                                </AllButton>
                                                                            </PagerSettings>
                                                                        </dx:ASPxDataView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td  style="padding-left:85px;" valign="top">
                                                                        <div id="divaudioPlayer" runat="server" visible="true">
                                                                               <table><tr><td><asp:Label ID="lblAudioplayer" runat="server"></asp:Label></td></tr>
                                                                               <tr><td><asp:LinkButton ID="lknDownload" Font-Underline="false" runat="server" 
                                                                                onclick="lknDownload_Click" >Download</asp:LinkButton></td>
                                                                               </tr></table>
                                                                                   
                                                                                
                                                                  
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <table cellpadding="2" cellspacing="3" border="0">
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    <img src='<%= PMToolWeb.AppSetting.GetSetting("ApplicationURL", PMToolWeb.AppSettingCategory.General) %>/images/comment.jpg'
                                                                                        alt="flag" />
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <span class="add-comment">Add a Comment</span>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <div class="comment-box">
                                                                            <dx:ASPxMemo ID="txtComment" runat="server" Height="110px" Width="375px" ClientInstanceName="memo">
                                                                                <ClientSideEvents KeyUp="function(s,e){var ele = s.GetText(); lblCommentCharRemain.SetText(750 - ele.length); OnMaxLengthChanged(s,e)} " />
                                                                            </dx:ASPxMemo>
                                                                            <div class="send-message">
                                                                                <asp:LinkButton ID="lnkbtnSendMessage" runat="server" CausesValidation="false" OnClick="lnkbtnSendMessage_Click">Send Message</asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <table cellpadding="2" cellspacing="3" border="0">
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    <div class="char-remain-box">
                                                                                        <dx:ASPxLabel ID="lblCommentCharRemain" runat="server" Text="750" ClientInstanceName="lblCommentCharRemain">
                                                                                        </dx:ASPxLabel>
                                                                                    </div>
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    Characters remaining
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <div class="task-desc">
                                                                            <span class="gray-text">Task Description</span><br />
                                                                            <br />
                                                                            <dx:ASPxLabel ID="lblTaskDescription" runat="server">
                                                                            </dx:ASPxLabel>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <dx:ASPxDataView ID="dvTaskComments" runat="server" RowPerPage="1000" EmptyDataText="[No comment for this task]"
                                                                            Width="96%" ColumnCount="1" ItemStyle-Paddings-Padding="2px" ItemSpacing="0px">
                                                                            <ItemTemplate>
                                                                                <div class="comment-item">
                                                                                    <div class="gray-text">
                                                                                        Updated:&nbsp;<%# Convert.ToDateTime(Eval("Createddate")).ToString(PMToolWeb.AppSetting.LongDateFormat) %></div>
                                                                                    <div class="gray-text">
                                                                                        Updated By:&nbsp;<%# Convert.ToString(Eval("FullName"))%></div>
                                                                                    <div class="comment">
                                                                                        <%# Convert.ToString(Eval("Comment"))%>
                                                                                    </div>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                            <PagerSettings>
                                                                                <AllButton Visible="True">
                                                                                </AllButton>
                                                                            </PagerSettings>
                                                                        </dx:ASPxDataView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <table cellpadding="2" cellspacing="3" border="0" width="100%">
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                </td>
                                                                                <td align="right" valign="top">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div>
                                                                <table cellpadding="2" cellspacing="3" border="0" width="100%">
                                                                    <tr>
                                                                        <td align="left" valign="top">
                                                                            <div class="previous-task">
                                                                                <asp:LinkButton ID="lnkbtnPreviousTask" runat="server" CausesValidation="false" OnClick="Navigation_Click"
                                                                                    CommandArgument="Previous">Previous Task</asp:LinkButton>
                                                                            </div>
                                                                        </td>
                                                                        <td align="center" valign="top">
                                                                            <div class="delete-task">
                                                                                <asp:LinkButton ID="lnkbtnDeleteTask" runat="server" CausesValidation="false" OnClick="Navigation_Click"
                                                                                    CommandArgument="Delete" OnClientClick="javascript:return confirm('Are you sure want to delete this task?');">Delete Task</asp:LinkButton>
                                                                            </div>
                                                                        </td>
                                                                        <td align="right" valign="top">
                                                                            <div class="next-task">
                                                                                <asp:LinkButton ID="lnkbtnNextTask" runat="server" CausesValidation="false" OnClick="Navigation_Click"
                                                                                    CommandArgument="Next">Next Task</asp:LinkButton>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
