﻿<%@ Page Title="Proman | Task Management Queue" Language="C#" MasterPageFile="~/masterpages/default.Master"
    AutoEventWireup="true" CodeBehind="task-list.aspx.cs" Inherits="PMToolWeb.task_list" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRatingControl" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
    <div class="task-detail">
        <div class="heading">
            <h2>
                Task Management Queue
            </h2>
        </div>
        <div class="search">
        <table><tr>
            <td>
            <dx:ASPxLabel ID="lblStartDate" runat="server" Text="From:">
            </dx:ASPxLabel>
            </td>
            <td>
              <dx:ASPxDateEdit ID="dtStartDate" runat="server" Width="100px">
                                <ValidationSettings Display="None" ErrorDisplayMode="None">
                                    <RequiredField IsRequired="True" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="function(s, e) {e.isValid = s.GetValue() != '0'; if(e.isValid != true) { e.errorText = 'Required';}}" />
                            </dx:ASPxDateEdit>
             </td>
            <td>
            <dx:ASPxLabel ID="lblEndDate" runat="server" Text="To:">
            </dx:ASPxLabel>
            </td>
            <td>
                 <dx:ASPxDateEdit ID="dtEndDate" runat="server" Width="100px">
                                <ValidationSettings Display="None" ErrorDisplayMode="None">
                                    <RequiredField IsRequired="True" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="function(s, e) {e.isValid = s.GetValue() != '0'; if(e.isValid != true) { e.errorText = 'Required';}}" />
                            </dx:ASPxDateEdit>
            </td>
            <td>
            <dx:ASPxLabel ID="lblTaskStatus" runat="server" Text="Task Status:">
            </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxComboBox ID="ddlTaskStatus" runat="server" SelectedIndex="1" 
                    ValueType="System.String" Width="100px">
                    <Items>
                        <dx:ListEditItem Text="--All--" Value="0" />
                        <dx:ListEditItem Text="Open" Value="1" Selected="True" />
                        <dx:ListEditItem Text="In Progress" Value="2" />
                        <dx:ListEditItem Text="Pending" Value="3" />
                        <dx:ListEditItem Text="Closed" Value="4" />
                    </Items>
                </dx:ASPxComboBox>
            </td>
            <td>
                <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" 
                  >
                </dx:ASPxButton>
            </td>
            </tr></table>
            
            
            
            
        </div>
        <div class="maindiv">
            <div class="subdiv">
                 <div class="div-message default-padding-left">
                    <asp:Label ID="lblStatus" runat="server" EnableViewState="false"></asp:Label>
                </div>
                <div class="task-list-grid">
                <dx:ASPxGridView ID="gvTaskList" ClientInstanceName="gvTaskList" runat="server" KeyFieldName="TaskId"
                    AutoGenerateColumns="False" Width="777px" 
                        OnHtmlRowCreated="gvTaskList_HtmlRowCreated" OnRowCommand="gvTaskList_RowCommand">
                    <Columns>
                        <dx:GridViewDataColumn Caption="View" Width="25px" VisibleIndex="0">
                            <DataItemTemplate>
                                <a href='task-detail.aspx?projectid=<%# Eval("ProjectId") %>&taskid=<%# Eval("TaskId") %>'>
                                    <img id="imgView" runat="server" src="~/images/view.png" alt="View" style="border: 0px;"
                                        title="View" /></a>
                              
                                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Rate This Task"  CommandName="tskRate" CommandArgument='<%# Eval("TaskId") %>'></dx:ASPxButton>
                            </DataItemTemplate>
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Last Name" FieldName="LastName" Width="65px" VisibleIndex="1"/>
                        <dx:GridViewDataColumn Caption="First Name" FieldName="FirstName" Width="65px" VisibleIndex="2"/>
                       

                         <dx:GridViewDataComboBoxColumn Caption="Project Name" FieldName="ProjectName" VisibleIndex="3">
                  <PropertiesComboBox  IncrementalFilteringMode="StartsWith"
                    DropDownStyle="DropDown"  />
                                        <DataItemTemplate>
                                                     <a href='task-detail.aspx?projectid=<%# Eval("ProjectId") %>' class="project-name">
                                    <%# Eval("ProjectName")%></a>
                            </DataItemTemplate>
               
                         </dx:GridViewDataComboBoxColumn>
                           <dx:GridViewDataColumn Caption="Status" FieldName="TaskStatus" Width="50" VisibleIndex="4">
                        <DataItemTemplate>
                            <%# Enum.Parse(typeof(PMToolWeb.TaskStatus), Convert.ToString(Eval("TaskStatus")))%>
                        </DataItemTemplate>
                               <CellStyle HorizontalAlign="Left">
                               </CellStyle>
                    </dx:GridViewDataColumn>
                      
                        <dx:GridViewDataColumn Caption="Due In" FieldName="DueIn" Width="55px" VisibleIndex="5" />
                        <dx:GridViewDataColumn Caption="Assigned By" FieldName="AssignByName" Width="100px" VisibleIndex="6" />
                        <dx:GridViewDataColumn Caption="WF ID" FieldName="WireframeNo" Width="60px" VisibleIndex="6" />
                        
                        <dx:GridViewDataColumn Caption="Days Open" FieldName="DaysOpen" Width="35px" VisibleIndex="7"/>
                        <dx:GridViewDataColumn Caption="Task ID" FieldName="TaskId" Width="45px" VisibleIndex="8">
                     
                                <DataItemTemplate>
                                     <a href='task-detail.aspx?projectid=<%# Eval("ProjectId") %>&taskid=<%# Eval("TaskId") %>' class="project-name">
                                    <%# Eval("TaskID")%></a>
                            </DataItemTemplate>
                     
                        </dx:GridViewDataColumn> 
                    </Columns>
                          <SettingsPager PageSize="3">
                    </SettingsPager>
                        <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                </dx:ASPxGridView>
                </div>
            </div>
        </div>
    </div>
    <dx:ASPxPopupControl CssClass="pcTastRating" ID="pcTastRating" runat="server" Modal="True"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="Above" Width="600px"
            HeaderText="Task Rating" CloseAction="CloseButton" PopupVerticalOffset="25" 
            ShowPageScrollbarWhenModal="True">
            <ContentCollection>
                <dx:PopupControlContentControl>
                   <center>
                          <table>
                            <tr>
                                <td class="pclefttd">
                                    Task ID:
                                </td>
                                <td colspan="3">
                                    <dx:ASPxLabel ID="lbltaskid" runat="server" Font-Bold="True" ForeColor="#6600FF">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td class="pclefttd">
                                    Rating :
                                </td>
                                <td colspan="3">
                                    <dx:ASPxRatingControl ID="RatingTask"  runat="server" EnableTheming="True" ItemCount="10" Theme="Metropolis" Value="0">
                                  
                                    </dx:ASPxRatingControl>
                                </td>
                                
                            </tr>
                              <tr>
                                <td class="pclefttd">
                                    Comment :
                                </td>
                                <td colspan="3">
                                   <dx:ASPxTextBox ID="txtratingreview" runat="server" Width="170px">
                                      <ValidationSettings Display="None" ErrorDisplayMode="None">
                                    <RequiredField IsRequired="True" ErrorText="Required" />
                                </ValidationSettings>
                                   </dx:ASPxTextBox>
                                </td>
                                
                            </tr>
                              <tr>
                                  <td>

                                  </td>
                                  <td>
   <dx:ASPxButton ID="submit" runat="server" Text="Save" OnClick="submit_Click" >
                                        </dx:ASPxButton>
                                  </td>
                              </tr>
                              </table>
                   </center>
                    <asp:HiddenField ID="hidtaskratingid" runat="server"></asp:HiddenField>
                    </dx:PopupControlContentControl>
                    </ContentCollection>
        </dx:ASPxPopupControl>
</asp:Content>
