﻿<%@ Page Title="Project Queue | Project Management Tool" Language="C#" MasterPageFile="~/masterpages/default.Master"
    AutoEventWireup="true" CodeBehind="project-queue.aspx.cs" Inherits="PMToolWeb.project_queue" %>

<%--<%@ Register Assembly="DevExpress.Xpo.v12.2.Web, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Xpo" TagPrefix="dx" %>--%>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .c
        {
            background: white;
            border: solid 1px gray;
            width: 180px;
            height: 50px;
            text-align: center;
            vertical-align: middle;
            padding-top: 3px;
            padding-left: 5px;
            display: none;
             line-height: 16px;
        }
        #date
        {
            margin-right: 6px;
            margin-top: 2px;
            padding: 2px;
            
        }
        .white
        {
            background: white;
        }
        .red
        {
            background: red;
        }
        .yellow
        {
            background: yellow;
        }
        .green
        {
            background: green;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
    <script type="text/javascript" src="scripts/jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#popup').hide();
        });

        var IE = document.all ? true : false;
        if (!IE) document.captureEvents(Event.MOUSEMOVE)
        document.onmousemove = getMouseXY;
        var pos_x = 0;
        var pos_y = 0;
        function getMouseXY(e) {
            if (IE) { // grab the x-y pos.s if browser is IE
                pos_x = event.clientX + document.documentElement.scrollLeft;
                pos_y = event.clientY + document.documentElement.scrollTop;
            }
            else {  // grab the x-y pos.s if browser is NS
                pos_x = e.pageX;
                pos_y = e.pageY;
            }
            if (pos_x < 0) { tempX = 0; }
            if (pos_y < 0) { tempY = 0; }


        }

        function DisableUnWantedPopUp(src) {

        }
        function OnMoreInfoClick(event, link, date, color, imageUrl,updatedby) {
            var ar = new Array(10);

            var str = imageUrl;

            var patt1 = /notaskimage.jpg/;

            ar = (str.match(patt1));
            if (ar != null)
            { return; }
            if (color == "white") {
                document.getElementById("date").style.backgroundColor = "white";
            }
            if (color == "red") {
                document.getElementById("date").style.backgroundColor = "#EE1B22";
            }
            if (color == "green") {
                document.getElementById("date").style.backgroundColor = "#03FD00";

            }
            if (color == "yellow") {
                document.getElementById("date").style.backgroundColor = "#FEF600";
            }
            //  alert("ecent=" + event + ":Link=" + link + ":Date=" + date + ":Color=" + color);
            document.getElementById("date").innerHTML = date + '<br><span style="padding-top:5px;">Updated By: ' + updatedby + '</span>' ;
            $('#popup').addClass('c');

            //            // $('#pp').add('Style','background:'+color+';position:absolute;left:'+(pos_x+25)+"px;top:"+(pos_y+22)+"px;");
            //            pos_x = event.offsetX ? (event.offsetX) : event.pageX - link.offsetLeft;
            //            pos_y = event.offsetY ? (event.offsetY) : event.pageY - link.offsetTop;


            document.getElementById("popup").setAttribute("Style", "position:absolute;left:" + (pos_x + 25) + "px; top:" + (pos_y + 22) + "px;");

            $('#popup').show(300);
            //popup.Show();

        }
    </script>
    <div class="project-queue">
        <div class="heading">
            <h2>
                Active Projects
            </h2>
        </div>
        <div class="div-message default-padding-left">
            <asp:Label ID="lblStatus" runat="server" EnableViewState="false"></asp:Label>
        <%--    <dx:XpoDataSource ID="XpoDataSource1" ServerMode="false" runat="server" 
                TypeName="PMToolWeb.Customer">
            </dx:XpoDataSource>--%>
         
        </div>
        <div class="Projects-list">
            <dx:ASPxGridView ID="gvProjects" ClientInstanceName="grid" runat="server" KeyFieldName="ProjectId"
                Width="777px" AutoGenerateColumns="False" OnRowCommand="gvProjects_RowCommand">
                <Columns>
                    <dx:GridViewDataColumn Caption="View" Width="50">
                        <DataItemTemplate>
                            <asp:LinkButton ID="lnkView" runat="server" CommandName="Edit" CausesValidation="false"
                                CommandArgument='<%# Eval("ProjectId") %>'>
                                <img id="Img1" runat="server" src="~/images/view.png" alt="View" style="border: 0px;"
                                    title="View" /></asp:LinkButton>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataTextColumn Caption="Project Name" FieldName="ProjectName" Width="300" />
                    <dx:GridViewDataColumn Caption="Open Tasks" FieldName="OpenTask" Width="20">
                        <DataItemTemplate>
                            <%#Eval("OpenTask")%>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataTextColumn Caption="Modules" FieldName="Modules" Width="60" />
                    <dx:GridViewDataColumn Caption="Status" FieldName="ProjectStatus" Width="50">
                        <DataItemTemplate>
                            <%# Enum.Parse(typeof(PMToolWeb.ProjectStatus), Convert.ToString(Eval("ProjectStatus")))%>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Project Completion Rate" FieldName="ProjectCompletionStatus"
                        Width="200">
                        <DataItemTemplate>
                           <table width="100%">
                           <tr>
                           <td width="70%" style="text-align:left;"><%# GetProjectCompletionStatus(Convert.ToInt32(Eval("ProjectCompletionStatus")))%></td>
                           <td width="30%" style=" color:Blue; font-weight:bold;"><%#Eval("ProjectCompletionStatus")+"%"%></td>
                           </tr>
                           </table>    
                             
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Edit" Width="50">
                        <DataItemTemplate>
                            <asp:LinkButton ID="lnkEdit" ForeColor="Blue" Font-Underline="true" runat="server"
                                CommandName="EditProject" CausesValidation="false" CommandArgument='<%# Eval("ProjectId") %>'>
                             Edit</asp:LinkButton>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>
                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
            </dx:ASPxGridView>
        </div>
        <div class="project-view" runat="server" id="divProjectView" visible="false">
            <div class="sub-heading">
                <div class="project-name">
                    <h2>
                        <span id="spanProjectName" runat="server"></span>
                    </h2>
                </div>
                <div class="day-to-project-due">
                    <h2>
                        <span id="spanDayToProjectDue" runat="server"></span>
                    </h2>
                </div>
            </div>
            <br />
            <table class="project-overallcompletion">
                <tr>
                    <td>
                        Overall Project completion Rate :
                    </td>
                    <td>
                        <dx:ASPxProgressBar ID="pbOverall" runat="server" Height="21px" Width="290px">
                        </dx:ASPxProgressBar>
                    </td>
                </tr>
            </table>
            <div style="width: 500px; padding-left: 15px">
                <hr class="hr" id="Hr1" runat="server" />
                <table class="project-legend" id="legend" runat="server">
                    <tr>
                        <td>
                            <b>Legend</b>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="lead-align-right">
                            01-
                        </td>
                        <td>
                            Brain Storming
                        </td>
                        <td class="lead-marginleft">
                            06-
                        </td>
                        <td>
                            Testing
                        </td>
                    </tr>
                    <tr>
                        <td class="lead-align-right">
                            02-
                        </td>
                        <td>
                            Discussion
                        </td>
                        <td class="lead-marginleft">
                            07-
                        </td>
                        <td>
                            Bug Found
                        </td>
                    </tr>
                    <tr>
                        <td class="lead-align-right">
                            03-
                        </td>
                        <td>
                            Debate
                        </td>
                        <td class="lead-marginleft">
                            08-
                        </td>
                        <td>
                            Bug Fixes
                        </td>
                    </tr>
                    <tr>
                        <td class="lead-align-right">
                            04-
                        </td>
                        <td>
                            Scoping
                        </td>
                        <td class="lead-marginleft">
                            09-
                        </td>
                        <td>
                            Awaiting Client Approval
                        </td>
                    </tr>
                    <tr>
                        <td class="lead-align-right">
                            05-
                        </td>
                        <td>
                            Programming
                        </td>
                        <td class="lead-marginleft">
                            10-
                        </td>
                        <td>
                            Closed
                        </td>
                    </tr>
                </table>
                <hr class="hr" id="hr2" runat="server" />
                <br />
            </div>
            <table runat="server" id="colordot" class="colordot">
                <tr>
                    <td>
                        <dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/images/bullet_green.png">
                        </dx:ASPxImage>
                    </td>
                    <td>
                        Last activity 1-48 hours ago
                    </td>
                    <td>
                        <dx:ASPxImage ID="ASPxImage2" runat="server" ImageUrl="~/images/bullet_yellow.png">
                        </dx:ASPxImage>
                    </td>
                    <td>
                        Last activity 49-72 hours ago
                    </td>
                    <td>
                        <dx:ASPxImage ID="ASPxImage3" runat="server" ImageUrl="~/images/bullet_red.png">
                        </dx:ASPxImage>
                    </td>
                    <td>
                        Last activity 73-96 hours ago
                    </td>
                    <td>
                        <dx:ASPxImage ID="ASPxImage4" runat="server" ImageUrl="~/images/bullet_black.png">
                        </dx:ASPxImage>
                    </td>
                    <td>
                        Last activity 96 or more hours ago
                    </td>
                </tr>
            </table>
            <div class="module-detail">
                <div class="grid-module-status">
                    <dx:ASPxGridView Visible="false" ID="gvModuleStatus" ClientInstanceName="gvModuleStatus" runat="server"
                        KeyFieldName="ModuleId" Width="100%" OnPageIndexChanged="gvModuleStatus_PageIndexChanged"
                        AutoGenerateColumns="False" ClientIDMode="AutoID" 
                        >
                        <Border BorderStyle="None" />
                        <Columns>
                            <dx:GridViewDataColumn Caption="View" Width="58.33%" CellStyle-HorizontalAlign="Right">
                                <DataItemTemplate>
                                    <%# "Module" + (Convert.ToInt64(Container.ItemIndex)+1) + ": " + Eval("ModuleName") + " : "%>
                                    <%--<%# Convert.ToString(Eval("ModuleName")) == "Overall Project completion Rate" ? Eval("ModuleName") + ": " : "Module " + Container.ItemIndex + ": " + Eval("ModuleName") + " : "%>--%>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataProgressBarColumn FieldName="ModuleCompletionStatus" />
                        </Columns>
                        <Templates>
                            <DetailRow>
                                <dx:ASPxGridView  SettingsDetail-ShowDetailRow="true" CssClass="componentGrid" ID="componentGrid"
                                    Width="100%" runat="server" KeyFieldName="ComponentId" OnDataBinding="component_DataBinding"
                                    OnBeforePerformDataSelect="detailGrid_DataSelect">
                                    <Columns>
                                        <dx:GridViewDataColumn Width="60%" CellStyle-HorizontalAlign="Right" FieldName="ComponentName"
                                            VisibleIndex="1">
                                            <DataItemTemplate>
                                                <%# "Component " + (Convert.ToInt64(Container.ItemIndex)+1) + ": " + Eval("ComponentName") + " : "%>
                                            </DataItemTemplate>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataProgressBarColumn Width="40%" FieldName="ComponentCompletionStatus"
                                            VisibleIndex="2">
                                        </dx:GridViewDataProgressBarColumn>
                                    </Columns>
                                    <Templates>
                                        <DetailRow>
                                            <dx:ASPxGridView ID="taskGrid" Width="100%" runat="server" KeyFieldName="TaskId"
                                                OnDataBinding="task_DataBinding" OnRowCommand="gvTasks_RowCommand">
                                                <Columns>
                                                    <dx:GridViewDataColumn Width="20px" CellStyle-ForeColor="Blue" FieldName="TaskId"
                                                        Caption="TaskId" VisibleIndex="1">
                                                        <DataItemTemplate>
                                                            <%--  <a runat="server" style="  text-decoration:underline;color:Blue;" href='<%#Eval("Link")%>' runat="server" id="link">
                                                                <%#Eval("TaskId")%>
                                                            </a>--%>
                                                            <asp:LinkButton ID="LinkButton1" ForeColor="Blue" Font-Underline="true" Enabled='<%#ValidateLink()%>'
                                                                runat="server" PostBackUrl='<%#Eval("Link")%>'><%#Eval("TaskId")%></asp:LinkButton>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="150px" FieldName="Title" Caption="TaskTitle" VisibleIndex="2">
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="13px" Caption="01" FieldName="TaskId" VisibleIndex="3">
                                                        <DataItemTemplate>
                                                            <img id="Img1" runat="server" src='<%#Eval("01")%>' onmouseover='<%#Eval("LastAcssesDate")%>'
                                                                style="border: 0px;" title="View" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="13px" FieldName="02" Caption="02" VisibleIndex="4">
                                                        <DataItemTemplate>
                                                            <img id="Img1" runat="server" src='<%#Eval("02")%>' onmouseover='<%#Eval("LastAcssesDate")%>'
                                                                style="border: 0px;" title="View" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="13px" FieldName="03" Caption="03" VisibleIndex="5">
                                                        <DataItemTemplate>
                                                            <img id="Img1" runat="server" src='<%#Eval("03")%>' onmouseover='<%#Eval("LastAcssesDate")%>'
                                                                style="border: 0px;" title="View" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="13px" FieldName="04" Caption="04" VisibleIndex="6">
                                                        <DataItemTemplate>
                                                            <img id="Img1" runat="server" src='<%#Eval("04")%>' onmouseover='<%#Eval("LastAcssesDate")%>'
                                                                style="border: 0px;" title="View" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="13px" FieldName="05" Caption="05" VisibleIndex="7">
                                                        <DataItemTemplate>
                                                            <img id="Img1" runat="server" src='<%#Eval("05")%>' onmouseover='<%#Eval("LastAcssesDate")%>'
                                                                style="border: 0px;" title="View" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="13px" FieldName="06" Caption="06" VisibleIndex="8">
                                                        <DataItemTemplate>
                                                            <img id="Img1" runat="server" src='<%#Eval("06")%>' onmouseover='<%#Eval("LastAcssesDate")%>'
                                                                style="border: 0px;" title="View" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="13px" FieldName="07" Caption="07" VisibleIndex="9">
                                                        <DataItemTemplate>
                                                            <img id="Img1" runat="server" src='<%#Eval("07")%>' onmouseover='<%#Eval("LastAcssesDate")%>'
                                                                style="border: 0px;" title="View" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="13px" FieldName="08" Caption="08" VisibleIndex="10">
                                                        <DataItemTemplate>
                                                            <img id="Img1" runat="server" src='<%#Eval("08")%>' onmouseover='<%#Eval("LastAcssesDate")%>'
                                                                style="border: 0px;" title="View" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="13px" FieldName="09" Caption="09" VisibleIndex="11">
                                                        <DataItemTemplate>
                                                            <img id="Img1" runat="server" src='<%#Eval("09")%>' onmouseover='<%#Eval("LastAcssesDate")%>'
                                                                style="border: 0px;" title="View" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="13px" FieldName="10" Caption="10" VisibleIndex="12">
                                                        <DataItemTemplate>
                                                            <img id="Img1" runat="server" src='<%#Eval("10")%>' onmouseover='<%#Eval("LastAcssesDate")%>'
                                                                style="border: 0px;" title="View" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords">
                                                    <Summary AllPagesText="Pages: {0} - {1} ({2} P)" Text="Page {0} of {1} ({2} Tasks)" />
                                                </SettingsPager>
                                            </dx:ASPxGridView>
                                        </DetailRow>
                                    </Templates>
                                    <SettingsPager Mode="ShowAllRecords">
                                    </SettingsPager>
                                    <Settings GridLines="None" ShowColumnHeaders="false" />
                                    <SettingsDetail ShowDetailRow="true" />
                                </dx:ASPxGridView>
                            </DetailRow>
                        </Templates>
                        <SettingsPager PageSize="1">
                            <Summary AllPagesText="Pages: {0} - {1} ({2} P)" Text="Page {0} of {1} ({2} Modules)" />
                        </SettingsPager>
                        <SettingsDetail ShowDetailRow="true" />
                        <Settings GridLines="None" ShowColumnHeaders="false" />
                    </dx:ASPxGridView>
                 
                    <%--    <hr class="hr" id="Hr1" runat="server" />--%>
                    <%--              <table class="project-legend" id="legend" runat="server">
                        <tr>
                            <td>
                                <b>Legend</b>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="lead-align-right">
                                01-
                            </td>
                            <td>
                                Brain Storming
                            </td>
                            <td class="lead-marginleft">
                                06-
                            </td>
                            <td>
                                Testing
                            </td>
                        </tr>
                        <tr>
                            <td class="lead-align-right">
                                02-
                            </td>
                            <td>
                                Discussion
                            </td>
                            <td class="lead-marginleft">
                                07-
                            </td>
                            <td>
                                Bug Found
                            </td>
                        </tr>
                        <tr>
                            <td class="lead-align-right">
                                03-
                            </td>
                            <td>
                                Debate
                            </td>
                            <td class="lead-marginleft">
                                08-
                            </td>
                            <td>
                                Bug Fixes
                            </td>
                        </tr>
                        <tr>
                            <td class="lead-align-right">
                                04-
                            </td>
                            <td>
                                Scoping
                            </td>
                            <td class="lead-marginleft">
                                09-
                            </td>
                            <td>
                                Awaiting Client Approval
                            </td>
                        </tr>
                        <tr>
                            <td class="lead-align-right">
                                05-
                            </td>
                            <td>
                                Programming
                            </td>
                            <td class="lead-marginleft">
                                10-
                            </td>
                            <td>
                                Closed
                            </td>
                        </tr>
                    </table>--%>
                    <%--   <hr class="hr" id="hr2" runat="server" />--%>
                    <br />
                    <%--     <table runat="server" id="colordot" class="colordot">
                        <tr>
                            <td>
                                <dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/images/bullet_green.png">
                                </dx:ASPxImage>
                            </td>
                            <td>
                                Last activity 1-48 hours ago
                            </td>
                            <td>
                                <dx:ASPxImage ID="ASPxImage2" runat="server" ImageUrl="~/images/bullet_yellow.png">
                                </dx:ASPxImage>
                            </td>
                            <td>
                                Last activity 49-72 hours ago
                            </td>
                            <td>
                                <dx:ASPxImage ID="ASPxImage3" runat="server" ImageUrl="~/images/bullet_red.png">
                                </dx:ASPxImage>
                            </td>
                            <td>
                                Last activity 73-96 hours ago
                            </td>
                            <td>
                                <dx:ASPxImage ID="ASPxImage4" runat="server" ImageUrl="~/images/bullet_black.png">
                                </dx:ASPxImage>
                            </td>
                            <td>
                                Last activity 96 or more hours ago
                            </td>
                        </tr>
                    </table>--%>
                </div>
                <div id="taskView" class="taskview" runat="server">
            
                </div>
                <div id="popup" style="height:40px;">
                    <div id="date" style="height: 35px;padding-top: 5px;">
                        &nbsp;</div>
                </div>
                <div class="buttons">
                    <table cellpadding="2" cellspacing="3">
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnViewTasks" runat="server" Text="View Tasks" OnClick="btnViewTasks_Click">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnEditProject" runat="server" Text="Edit Project" OnClick="btnProjectEdit_Click">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
