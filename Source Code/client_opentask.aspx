﻿<%@ Page Title="Create Task" Language="C#" MasterPageFile="~/masterpages/default.Master"
    AutoEventWireup="true" CodeBehind="client_opentask.aspx.cs" Inherits="PMToolWeb.client_opentask" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register TagPrefix="uc" TagName="clientHeader" Src="~/controls/clientHeaderControl.ascx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
    <script type="text/javascript">

        function OnSelectedIndexChanged(ddlProjectName) {
            ddlModule.PerformCallback(ddlProjectName.GetValue().toString());
        }
        function OnSelectedIndexChanged1(ddlModule) {
            ddlComp.PerformCallback(ddlModule.GetValue().toString());
        }
    </script>
    <table class="mainTable" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" EnableViewState="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="100%" runat="server" id="tblOpenTask"
                    visible="false">
                    <tr>
                        <td>
                            <table cellpadding="0" cellpadding="0" width="100%">
                                <tr>
                                    <td width="50%" class="TaskNumberLabel">
                                        <asp:Label runat="server" CssClass="label" ID="lblTaskNumber" Text="New Task"></asp:Label>
                                    </td>
                                    <td width="50%" class="headerLinks">
                                        <a href="client_opentask.aspx">Home</a> &nbsp;| &nbsp; <a href="client_opentask.aspx?status=open">
                                            Open New Task</a> &nbsp;| &nbsp; <a href="client_opentask.aspx?status=closed">View Closed
                                                Tasks</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="100%" class="createTaskMainTable">
                                <tr>
                                    <td class="left-td">
                                    </td>
                                    <td class="right-td">
                                        (<span class="red">*</span> Mandatory Response Required.)
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                        <span class="red">*</span> What is the Project Name:
                                    </td>
                                    <td class="right-td">
                                        <dx:ASPxComboBox ID="ddlProject" Width="300px" runat="server">
                                            <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            </ValidationSettings>
                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { OnSelectedIndexChanged(s); }"
                                                Validation="function(s, e) {e.isValid = s.GetValue() != '0'; if(e.isValid != true) { e.errorText = 'Required';}}" />
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                        <span class="red">*</span> What is the name of the Module:
                                    </td>
                                    <td class="right-td">
                                        <dx:ASPxComboBox ID="ddlModule" Width="300px" runat="server" OnCallback="ddlModule_Callback"
                                            ClientInstanceName="ddlModule">
                                            <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            </ValidationSettings>
                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { OnSelectedIndexChanged1(s); }"
                                                Validation="function(s, e) {e.isValid = s.GetValue() != '0'; if(e.isValid != true) { e.errorText = 'Required';}}" />
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                        <span class="red">*</span> Component Name or Description:
                                    </td>
                                    <td class="right-td">
                                        <dx:ASPxComboBox ID="ddlComponent" ClientInstanceName="ddlComp" Width="300px" OnCallback="ddlComponent_Callback"
                                            runat="server">
                                            <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="function(s, e) {e.isValid = s.GetValue() != '0'; if(e.isValid != true) { e.errorText = 'Required';}}" />
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                        What is the Wireframe ID, if known:
                                    </td>
                                    <td class="right-td">
                                        <dx:ASPxTextBox ID="txtWfID" runat="server" Width="150px">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                    </td>
                                    <td class="grayTextright-td">
                                        (Provide a short description of the task)
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                        <span class="red">*</span> Subject:
                                    </td>
                                    <td class="right-td">
                                        <dx:ASPxTextBox ID="txtSubject" runat="server" Width="464px">
                                            <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                                <RequiredField IsRequired="True" ErrorText="Required" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td" style="vertical-align: top;">
                                        <span class="red">*</span> Why is this task being opened?
                                    </td>
                                    <td class="right-td">
                                        <dx:ASPxRadioButtonList ID="rdListReason" Width="464px" runat="server">
                                        </dx:ASPxRadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td" style="vertical-align: top">
                                        <span class="red">*</span> Describe the work needing to be completed to complete
                                        the task:
                                    </td>
                                    <td class="right-td">
                                        <dx:ASPxMemo ID="txtTaskDescriptnio" runat="server" Height="175px" Width="464px">
                                            <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                                <RequiredField IsRequired="True" ErrorText="Required" />
                                            </ValidationSettings>
                                        </dx:ASPxMemo>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="separateDivBlue">
                                        <div>
                                            &nbsp;
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding-left: 15px; padding-right: 8px; padding-bottom: 13px;
                                        padding-top: 7px;">
                                        Upload a [jpg] image or [pdf] file to help support or explain the work associated
                                        with this task. You may upload a JPG or .PDF file format only. If you need help,
                                        please contact mxicoders.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table>
                                            <tr>
                                                <td style="padding-left: 80px" valign="top">
                                                    <dx:ASPxUploadControl ID="fuTaskImage" runat="server" Width="300px" Size="50">
                                                    </dx:ASPxUploadControl>
                                                </td>
                                                <td valign="top">
                                                    <dx:ASPxButton ID="btnUpload" ForeColor="Black" runat="server" OnClick="btnUpload_Click"
                                                        Text="Upload" CausesValidation="false">
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td align="left" valign="top" style="padding-bottom: 10px;">
                                        <dx:ASPxDataView SkinID="dsd" ItemStyle-Width="140px" ID="dvTaskImages" runat="server"
                                            RowPerPage="3" OnItemCommand="dvTaskImages_ItemCommand" ItemStyle-Paddings-Padding="0px"
                                            ItemSpacing="10px" EmptyDataText="[No Image uploaded]" ColumnCount="3">
                                            <ItemTemplate>
                                                <div class="item-image">
                                                    <dx:ASPxImage ID="imgTaskImage" runat="server" ImageUrl='<%# PMToolWeb.Helper.GetTaskImage(this.TaskID, Convert.ToString(Eval("ImageName"))) %>'
                                                        Width="127" Height="127" />
                                                </div>
                                                <table cellpadding="2" cellspacing="3" width="100%">
                                                    <tr>
                                                        <td align="left" valign="middle">
                                                            <asp:LinkButton ID="lnkbtnTaskImageDelete" runat="server" CommandName="Delete" CommandArgument='<%# Eval("TaskImagesId") %>'
                                                                OnClientClick="return confirm('Are you sure want delete this task image?');"
                                                                CausesValidation="false" CssClass="deletebutton">Delete</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <PagerSettings>
                                                <AllButton Visible="True">
                                                </AllButton>
                                            </PagerSettings>
                                        </dx:ASPxDataView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <img src="images/spacer.gif" width="4" height="9" alt="" />
                                        <dx:ASPxButton ID="btnOpenTask" SkinID="dummy" runat="server" Image-Url="~/images/openTask.png"
                                            OnClick="btnOpenTask_Click">
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="images/spacer.gif" width="4" height="19" alt="" />
                        </td>
                    </tr>
                    <%--  <tr>
                        <td class="headerLinks">
                            <a href="client_opentask.aspx">Home</a> &nbsp;| &nbsp; <a href="client_opentask.aspx?status=open">
                                Open New Task</a> &nbsp;| &nbsp; <a href="client_opentask.aspx?status=closed">View Closed
                                    Tasks</a>
                        </td>
                    </tr>--%>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <%--     SECOND DIV CLOSED TASK--%>
                <table cellpadding="0" cellspacing="0" width="100%" id="tblClosedTask" runat="server"
                    visible="false">
                    <tr>
                        <td>
                            <table cellpadding="0" cellpadding="0" width="100%">
                                <tr>
                                    <td width="50%" class="TaskNumberLabel">
                                        <asp:Label runat="server" CssClass="label" ID="lblEditTasknumber" Text="New Task"></asp:Label>
                                    </td>
                                    <td width="50%" class="headerLinks">
                                        <a href="client_opentask.aspx">Home</a> &nbsp;| &nbsp; <a href="client_opentask.aspx?status=open">
                                            Open New Task</a> &nbsp;| &nbsp; <a href="client_opentask.aspx?status=closed">View Closed
                                                Tasks</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="100%" class="createTaskMainTable">
                                <tr>
                                    <td colspan="2" runat="server" id="tdclosedTask" visible="false">
                                        <table>
                                            <tr>
                                                <td>
                                                    <img src="images/closeMark.png" width="18px" />
                                                </td>
                                                <td valign="middle">
                                                    <asp:Label ID="lblclosedTaskDate" ForeColor="#C00000" Font-Bold="true" runat="server"
                                                        Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                        Task Created By:
                                    </td>
                                    <td class="right-td">
                                        <asp:Label runat="server" ID="lblTaskCreatedBy"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                        What is the Project Name:
                                    </td>
                                    <td class="right-td">
                                        <asp:Label runat="server" ID="lblProjectName"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                        What is the name of the Module:
                                    </td>
                                    <td class="right-td">
                                        <asp:Label runat="server" ID="lblModuleName"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                        Component Name or Description:
                                    </td>
                                    <td class="right-td">
                                        <asp:Label runat="server" ID="lblComponentName"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                        What is the Wireframe ID, if known:
                                    </td>
                                    <td class="right-td">
                                        <asp:Label runat="server" ID="lblWireframeId"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td">
                                        Subject:
                                    </td>
                                    <td class="right-td">
                                        <asp:Label runat="server" ID="lblSubject"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td" style="vertical-align: top;">
                                        Why is this task being opened?
                                    </td>
                                    <td class="right-td">
                                        <asp:Label runat="server" ID="lblTaskopebReason"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="left-td" style="vertical-align: top">
                                        Work Description:
                                    </td>
                                    <td class="right-td">
                                        <asp:Label runat="server" ID="lblTaskDescription" Width="490px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td align="left" valign="top" colspan="1">
                                        <dx:ASPxDataView ItemStyle-Width="140px" SkinID="sdF" ID="dsTaskImages1" runat="server"
                                            RowPerPage="3" OnItemCommand="dvTaskImages_ItemCommand" ItemStyle-Paddings-Padding="0px"
                                            ItemSpacing="0px" EmptyDataText="[No Image uploaded]" ColumnCount="3" ItemStyle-Height="140px">
                                            <ItemTemplate>
                                                <div class="item-image">
                                                    <a href='<%#  ResolveClientUrl(PMToolWeb.Helper.GetTaskImage(this.TaskID, Convert.ToString(Eval("ImageName")))) %>'
                                                        target="_blank">
                                                        <dx:ASPxImage ID="imgTaskImage" runat="server" ImageUrl='<%# PMToolWeb.Helper.GetTaskImage(this.TaskID, Convert.ToString(Eval("ImageName"))) %>'
                                                            Width="127" Height="127" />
                                                    </a>
                                                </div>
                                            </ItemTemplate>
                                            <PagerSettings>
                                                <AllButton Visible="True">
                                                </AllButton>
                                            </PagerSettings>
                                        </dx:ASPxDataView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="separateDivBlue">
                                        <div>
                                            &nbsp;
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table cellpadding="0" cellspacing="0" width="100%" class="tblUpdate" runat="server"
                                            visible="true" id="tblCommentTable">
                                            <tr>
                                                <td valign="top">
                                                    <img src="images/updateClock.png" width="50px" />
                                                </td>
                                                <td valign="top" style="padding-top: 6px; padding-left: 4px;">
                                                    <b>Send
                                                        <br />
                                                        Update:</b>
                                                </td>
                                                <td class="memo">
                                                    <dx:ASPxMemo ID="txtComment" runat="server" Height="100px" Width="450px">
                                                        <ValidationSettings ValidationGroup="comment">
                                                            <RequiredField IsRequired="true" />
                                                        </ValidationSettings>
                                                    </dx:ASPxMemo>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td style="text-align: right; padding-left: 315px;">
                                                    <dx:ASPxButton OnClick="btnSendUpdate_Click" SkinID="dummy" ValidationGroup="comment"
                                                        ID="btnSendUpdate" Image-Url="~/images/sendupdate.png" runat="server" Text="">
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:DataList OnItemCommand="dlComment_ItemCommand" ID="dlComments" Width="100%"
                                            CssClass="dlComments" runat="server">
                                            <ItemTemplate>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="separateDivBlue">
                                                            <div>
                                                                &nbsp;
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="details">
                                                            <asp:Label ID="Label1" runat="server"></asp:Label>
                                                            <span class="light">Sent by: &nbsp;<%#Eval("FullName")%></span><br /><span class="light"><%#PMToolWeb.Helper.clientDateFormate(Convert.ToDateTime(Eval("Createddate")))%></span><br /><span class="dark"><asp:Label ID="Label4" runat="server" Text='<%#Eval("Comment")%>'></asp:Label></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="details">
                                                            <asp:LinkButton ID="lnkDelete" Visible='<%#checkDeleteComment()%>' ForeColor="Blue" Font-Underline="true" runat="server"
                                                                CommandName="Delete" OnClientClick="return confirm('Are you sure want to delete this comment?');" CausesValidation="false" CommandArgument='<%# Eval("TaskCommentId") %>'>
                            Delete</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="images/spacer.gif" width="4" height="7" alt="" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td style="padding-left: 15px;">
                                        <img src="images/spacer.gif" width="4" height="9" alt="" />
                                        <dx:ASPxButton ID="btnCloseTask" SkinID="dummy" runat="server" Image-Url="~/images/closeTask1.png"
                                            Visible="false" OnClick="btnCloseTask_Click">
                                        </dx:ASPxButton>
                                        <dx:ASPxButton ID="btnReturnToTask" SkinID="dummy" runat="server" Image-Url="~/images/returnTask.png"
                                            OnClick="btnReturnTask_Click" Visible="false">
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="images/spacer.gif" width="4" height="19" alt="" />
                        </td>
                    </tr>
                    <%--  <tr>
                        <td class="headerLinks">
                            <a href="client_opentask.aspx">Home</a> &nbsp;| &nbsp; <a href="client_opentask.aspx?status=open">
                                Open New Task</a> &nbsp;| &nbsp; <a href="client_opentask.aspx?status=closed">View Closed
                                    Tasks</a>
                        </td>
                    </tr>--%>
                </table>
            </td>
        </tr>
        <tr>
            <td width="50%" runat="server" id="headerLinksForMainPage" class="headerLinks">
                <a href="client_opentask.aspx">Home</a> &nbsp;| &nbsp; <a href="client_opentask.aspx?status=open">
                    Open New Task</a> &nbsp;| &nbsp; <a href="client_opentask.aspx?status=closed">View Closed
                        Tasks</a>
            </td>
        </tr>
        <tr runat="server" id="activegrid1">
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="breadcumb">
                    <tr>
                        <td>
                            <b>Active Tasks<dx:ASPxLabel ID="lblTotalTask" runat="server" Text="">
                            </dx:ASPxLabel>
                            </b>
                        </td>
                        <td width="4" class="none">
                            <img src="images/spacer.gif" width="4" height="1" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr runat="server" id="activegrid2">
            <td>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <img src="images/spacer.gif" width="4" height="1" alt="" />
                            <dx:ASPxGridView Width="100%" ID="gvActiveTask" KeyFieldName="TaskId" runat="server"
                                OnRowCommand="gvTask_RowCommand">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="View" VisibleIndex="0" Width="15px">
                                        <DataItemTemplate>
                                            <asp:LinkButton ForeColor="Blue" Font-Underline="true" CommandName="Edit" CommandArgument='<%# Eval("TaskId") %>'
                                                ID="imgbtnEdit" runat="server">
                                                <img id="imgView" runat="server" src="~/images/view.png" alt="View" style="border: 0px;"
                                                    title="View" /></asp:LinkButton>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="Open Date" FieldName="Createddate" Width="55px" VisibleIndex="1" />
                                    <dx:GridViewDataColumn Caption="Module Name" FieldName="ModuleName" Width="150px"
                                        VisibleIndex="2" />
                                    <dx:GridViewDataColumn Caption="Task Number" CellStyle-HorizontalAlign="Left" Width="40px"
                                        FieldName="TaskId" VisibleIndex="3">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="Subject" FieldName="Title" Width="150px" VisibleIndex="5" />
                                    <dx:GridViewDataColumn Caption="Updates" FieldName="update" Width="25px" VisibleIndex="6" />
                                           <dx:GridViewDataColumn Caption="Attachment" FieldName="Attachment" Width="25px" VisibleIndex="6" >
                                           <DataItemTemplate>
                                            <asp:LinkButton ForeColor="Blue" Visible='<%#GetVisible(Convert.ToInt32(Eval("Attachment")))%>' Font-Underline="true" CommandName="Edit" ID="imgbtnattach" CommandArgument='<%# Eval("TaskId") %>' runat="server">
                                                <img id="imgView" runat="server" src='<%#GetAttachURL(Convert.ToInt32(Eval("Attachment")))%>' alt="View" style="border: 0px;"
                                                    title="View" /></asp:LinkButton>
                                           </DataItemTemplate>

                                           </dx:GridViewDataColumn>

                                </Columns>
                                <SettingsPager PageSize="10" Position="Bottom" SEOFriendly="Disabled" AlwaysShowPager="True">
                                    <Summary AllPagesText="omotional events" Text="Page {0} of {1} ({2} Tasks)" />
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr runat="server" id="closedTr1">
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="breadcumb">
                    <tr>
                        <td>
                            <b>Closed Tasks<dx:ASPxLabel ID="lblClosedTask" runat="server" Text="">
                            </dx:ASPxLabel>
                            </b>
                        </td>
                        <td width="4" class="none">
                            <img src="images/spacer.gif" width="4" height="1" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr runat="server" id="closedtr2">
            <td>
                <img src="images/spacer.gif" width="4" height="1" alt="" />
                <dx:ASPxGridView Width="100%" ID="gvClosedTask" KeyFieldName="TaskId" runat="server"
                    OnRowCommand="gvTask_RowCommand">
                    <Columns>
                        <dx:GridViewDataColumn Caption="View" VisibleIndex="0" Width="15px">
                            <DataItemTemplate>
                                <asp:LinkButton ForeColor="Blue" Font-Underline="true" CommandName="Edit" CommandArgument='<%# Eval("TaskId") %>'
                                    ID="imgbtnEdit" runat="server">
                                    <img id="imgView" runat="server" src="~/images/view.png" alt="View" style="border: 0px;"
                                        title="View" /></asp:LinkButton>
                            </DataItemTemplate>
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Open Date" FieldName="Createddate" Width="55px" VisibleIndex="1" />
                        <dx:GridViewDataColumn Caption="Module Name" FieldName="ModuleName" Width="150px"
                            VisibleIndex="2" />
                        <dx:GridViewDataColumn Caption="Task Number" CellStyle-HorizontalAlign="Left" Width="40px"
                            FieldName="TaskId" VisibleIndex="3">
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Subject" FieldName="Title" Width="150px" VisibleIndex="5" />
                        <dx:GridViewDataColumn Caption="Updates" FieldName="update" Width="25px" VisibleIndex="6" />
                    </Columns>
                    <SettingsPager PageSize="10" Position="Bottom" SEOFriendly="Disabled" AlwaysShowPager="True">
                        <Summary AllPagesText="omotional events" Text="Page {0} of {1} ({2} Tasks)" />
                    </SettingsPager>
                </dx:ASPxGridView>
            </td>
        </tr>
    </table>
</asp:Content>
