﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/default.Master" AutoEventWireup="true" CodeBehind="GridReport.aspx.cs" Inherits="PMToolWeb.GridReport" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
    <table class="auto-style1">
        <tr>
            <td>Select Role</td>
            <td>
                <dx:ASPxComboBox ID="ddlRole" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" TextField="RoleName" ValueField="RoleId">
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="SELECT [RoleId], [RoleName] FROM [Roles] ORDER BY [DisplayOrder]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Select Person</td>
            <td class="auto-style2">
                <dx:ASPxComboBox ID="ddlperson" runat="server" DataSourceID="SqlDataSource2" TextField="FirstName" ValueField="UserId">
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="SELECT [FirstName], [UserId] FROM [Users] WHERE ([RoleId] = @RoleId)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlRole" Name="RoleId" PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>Select Status</td>
            <td>
                 <dx:ASPxComboBox ID="ddlTaskStatus" runat="server" SelectedIndex="1" 
                    ValueType="System.String" Width="100px">
                    <Items>
                        <dx:ListEditItem Text="--All--" Value="0" />
                        <dx:ListEditItem Text="Open" Value="1" Selected="True" />
                        <dx:ListEditItem Text="In Progress" Value="2" />
                        <dx:ListEditItem Text="Pending" Value="3" />
                        <dx:ListEditItem Text="Closed" Value="4" />
                    </Items>
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <dx:ASPxButton ID="btnshowreport" runat="server" OnClick="btnshowreport_Click" Text="Show Report" Width="153px">
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                
               
            </td>
        </tr>
    </table>
    <dx:ASPxGridView ID="gvTaskList" ClientInstanceName="gvTaskList" runat="server" KeyFieldName="TaskId"
                    AutoGenerateColumns="False" Width="777px" 
                       >
                    <Columns>
                        
                        <dx:GridViewDataColumn Caption="Last Name" FieldName="LastName" Width="65px" VisibleIndex="1"/>
                        <dx:GridViewDataColumn Caption="First Name" FieldName="FirstName" Width="65px" VisibleIndex="2"/>
                       

                         <dx:GridViewDataComboBoxColumn Caption="Project Name" FieldName="ProjectName" VisibleIndex="3">
                  <PropertiesComboBox  IncrementalFilteringMode="StartsWith"
                    DropDownStyle="DropDown"  />
                                        <DataItemTemplate>
                                                     <a href='task-detail.aspx?projectid=<%# Eval("ProjectId") %>'  style="color:blue;" >
                                    <%# Eval("ProjectName")%></a>
                            </DataItemTemplate>
               
                         </dx:GridViewDataComboBoxColumn>
                           <dx:GridViewDataColumn Caption="Status" FieldName="TaskStatus" Width="50" VisibleIndex="4">
                        <DataItemTemplate>
                            <%# Enum.Parse(typeof(PMToolWeb.TaskStatus), Convert.ToString(Eval("TaskStatus")))%>
                        </DataItemTemplate>
                               <CellStyle HorizontalAlign="Left">
                               </CellStyle>
                    </dx:GridViewDataColumn>
                      
                        <dx:GridViewDataColumn Caption="Due In" FieldName="DueIn" Width="55px" VisibleIndex="5" />
                        <dx:GridViewDataColumn Caption="Assigned By" FieldName="AssignByName" Width="100px" VisibleIndex="6" />
                        <dx:GridViewDataColumn Caption="WF ID" FieldName="WireframeNo" Width="60px" VisibleIndex="6" />
                        
                        <dx:GridViewDataColumn Caption="Days Open" FieldName="DaysOpen" Width="35px" VisibleIndex="7"/>
                        <dx:GridViewDataColumn Caption="Task ID" FieldName="TaskId" Width="45px" VisibleIndex="8">
                     
                                <DataItemTemplate>
                                     <a href='task-detail.aspx?projectid=<%# Eval("ProjectId") %>&taskid=<%# Eval("TaskId") %>' class="project-name">
                                    <%# Eval("TaskID")%></a>
                            </DataItemTemplate>
                     
                        </dx:GridViewDataColumn> 
                    </Columns>
                          <SettingsPager PageSize="3">
                    </SettingsPager>
                        <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                </dx:ASPxGridView>
</asp:Content>
