﻿<%@ Page Title="Proman | My Private Tasks" Language="C#" MasterPageFile="~/masterpages/default.Master"
    AutoEventWireup="true" CodeBehind="my-private-task.aspx.cs" Inherits="PMToolWeb.my_private_task" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
    <div class="task-detail">
        <div class="heading">
            <h2>
                My Private Tasks
            </h2>
        </div>
        <div class="search">
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblStartDate" runat="server" Text="From:">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dtStartDate" runat="server" Width="100px">
                            <ValidationSettings Display="None" ErrorDisplayMode="None">
                                <RequiredField IsRequired="True" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="function(s, e) {e.isValid = s.GetValue() != '0'; if(e.isValid != true) { e.errorText = 'Required';}}" />
                        </dx:ASPxDateEdit>
                    </td>
                    <td>
                        <dx:ASPxLabel ID="lblEndDate" runat="server" Text="To:">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dtEndDate" runat="server" Width="100px">
                            <ValidationSettings Display="None" ErrorDisplayMode="None">
                                <RequiredField IsRequired="True" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="function(s, e) {e.isValid = s.GetValue() != '0'; if(e.isValid != true) { e.errorText = 'Required';}}" />
                        </dx:ASPxDateEdit>
                    </td>
                    <td>
                        <dx:ASPxLabel ID="lblTaskStatus" runat="server" Text="Task Status:">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="ddlTaskStatus" runat="server" SelectedIndex="0" ValueType="System.String"
                            Width="100px">
                            <Items>
                                <dx:ListEditItem Text="--All--" Value="0" />
                                <dx:ListEditItem Selected="True" Text="Open" Value="1" />
                                <dx:ListEditItem Text="In Progress" Value="2" />
                                <dx:ListEditItem Text="Pending" Value="3" />
                                <dx:ListEditItem Text="Closed" Value="4" />
                            </Items>
                        </dx:ASPxComboBox>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnSearch" runat="server" Text="Search">
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <div class="maindiv">
            <div class="subdiv">
                <div class="div-message default-padding-left">
                    <asp:Label ID="lblStatus" runat="server" EnableViewState="false"></asp:Label>
                </div>
                <div class="task-list-grid">
                    <dx:ASPxGridView ID="gvTaskList" ClientInstanceName="gvTaskList" runat="server" KeyFieldName="TaskId"
                        AutoGenerateColumns="False" Width="777px" OnHtmlRowCreated="gvTaskList_HtmlRowCreated">
                        <Columns>
                            <dx:GridViewDataColumn Caption="View" VisibleIndex="0" Width="25px">
                                <DataItemTemplate>
                                    <a href='task-detail.aspx?projectid=<%# Eval("ProjectId") %>&taskid=<%# Eval("TaskId") %>'>
                                        <img id="imgView" runat="server" src="~/images/view.png" alt="View" style="border: 0px;"
                                            title="View" /></a>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Last Name" FieldName="LastName" Width="65px" VisibleIndex="1" />
                            <dx:GridViewDataColumn Caption="First Name" FieldName="FirstName" Width="65px" VisibleIndex="2" />
                            <dx:GridViewDataColumn Caption="Project Name" Width="105px" FieldName="ProjectName"
                                VisibleIndex="3">
                                <DataItemTemplate>
                                    <a href='task-detail.aspx?projectid=<%# Eval("ProjectId") %>' class="project-name">
                                        <%# Eval("ProjectName")%></a>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Status" FieldName="TaskStatus" Width="45px" VisibleIndex="4">
                                <EditCellStyle HorizontalAlign="Left">
                                </EditCellStyle>
                                <DataItemTemplate>
                                    <%# Enum.Parse(typeof(PMToolWeb.TaskStatus), Convert.ToString(Eval("TaskStatus")))%>
                                </DataItemTemplate>
                                <CellStyle HorizontalAlign="Left">
                                </CellStyle>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Due In" FieldName="DueIn" Width="45px" VisibleIndex="5" />
                            <dx:GridViewDataColumn Caption="Assigned By" FieldName="AssignByName" Width="100px"
                                VisibleIndex="6" />
                            <dx:GridViewDataColumn Caption="Days Open" FieldName="DaysOpen" Width="45px" VisibleIndex="7" />
                            <dx:GridViewDataColumn Caption="Task ID" FieldName="TaskId" Width="45px" VisibleIndex="8" >
                            
                                <DataItemTemplate>
                                     <a href='task-detail.aspx?projectid=<%# Eval("ProjectId") %>&taskid=<%# Eval("TaskId") %>' class="project-name">
                                    <%# Eval("TaskID")%></a>
                            </DataItemTemplate>
                            </dx:GridViewDataColumn>
                        </Columns>
                        <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                    </dx:ASPxGridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
