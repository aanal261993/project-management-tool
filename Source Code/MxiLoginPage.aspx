﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MxiLoginPage.aspx.cs"  Inherits="PMToolWeb.MxiLoginPage" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../App_Themes/default/style.css" type="text/css"   /> 
    <style>
        *
        {
            font-family: Arial;
            font-size: 14px;
        }
    </style>
    <script type="text/javascript" src="scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript">
   
//        function validate() {
//            var mainMessage = $.trim($("#txtUserName").val());
//            var passMessage = $.trim($('#txtPass').val());

//            if (mainMessage.length <= 0) {
//                $('#messageDiv').html('<span style="color:red; font-size:12px;"> - Enter User Name.</span>');
//                return false;
//               
//            }
//            if (passMessage.length <= 0) {
//                $('#messageDiv').html('<span style="color:red;font-size:12px;"> - Enter Password.</span>');

//                return false;
//            }

//            else {
//                $('#messageDiv').html('');
//            }
//        }
    </script>
</head>
<body>
    <form id="form1" onsubmit="return validate();" runat="server">
    <div>
        <table width="170px" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" style="background: #0066FF; color: White; font-weight: bold; padding: 3px;">
                    Client Projects
                </td>
            </tr>
            <tr>
                <td style="background: #EAEAEA;">
                    <table cellpadding="0" cellspacing="0" width="100%" style="padding-left: 3px; padding-top: 4px;">
                        <tr>
                            <td style="padding-bottom:8px;">
                                                                 <asp:Label ID="lblStatus" runat="server" EnableViewState="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style=" padding-bottom:3px;">
                                <b style="font-size: 13px; padding: 4px;">User Name:</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 4px; padding-right: 4px;">

                                <dx:ASPxTextBox ID="txtUName" runat="server" Width="157px">
                                <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:5px; padding-bottom:3px;">
                                <b style="font-size: 13px; padding: 4px; ">Password:</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 4px; padding-right: 4px;">
                                <dx:ASPxTextBox ID="txtPass" runat="server" Width="157px">
                                <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="padding: 10px; padding-top:15px;">
                            
                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/MxiwLogin.png" 
                                    ForeColor="White" Width="78px" Height="24px" runat="server" 
                                    onclick="ImageButton1_Click"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
