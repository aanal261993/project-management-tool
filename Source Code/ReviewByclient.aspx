﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/default.Master" AutoEventWireup="true" CodeBehind="ReviewByclient.aspx.cs" Inherits="PMToolWeb.Review" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
        DataSourceID="SqlDataSource1" KeyFieldName="TaskReviewID">
        <Columns>
            <dx:GridViewDataTextColumn FieldName="TaskReviewID" ReadOnly="True" 
                VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="UserName" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ReviewRating" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="TaskId" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ReviewComment" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="CreatedBy" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="CreatedDate" VisibleIndex="6">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="Modifiedby" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="ModifyDate" VisibleIndex="8">
            </dx:GridViewDataDateColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:Connection String %>" 
        SelectCommand="if(@Roleid=1)
SELECT	[TaskReviewID],    [USers].UserName,     [ReviewRating],     [TaskId],     [ReviewComment],     [TaskReviewTab].CreatedBy,     TaskReviewTab.CreatedDate,     [TaskReviewTab].Modifiedby,     [ModifyDate]   
FROM	[TaskReviewTab]	INNER JOIN [Users]    ON [Users].[UserID] =[TaskReviewTab].[UserID]
ORDER BY TaskReviewTab.CreatedDate DESC
else
SELECT	[TaskReviewID],    [Users].UserName,     [ReviewRating],     [TaskId],     [ReviewComment],     [TaskReviewTab].CreatedBy,     TaskReviewTab.CreatedDate,     [TaskReviewTab].Modifiedby,     [ModifyDate]   
FROM	[TaskReviewTab]	INNER JOIN [Users]    ON [Users].[UserID] =[TaskReviewTab].[UserID]
where [TaskReviewTab].[UserID]=@Userid ORDER BY TaskReviewTab.CreatedDate DESC">
        <SelectParameters>
            <asp:SessionParameter Name="Roleid" SessionField="RoleId" />
            <asp:SessionParameter Name="Userid" SessionField="UserId" />
        </SelectParameters>
    </asp:SqlDataSource>
    </asp:Content>
