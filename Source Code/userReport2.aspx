﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/default.Master" AutoEventWireup="true" CodeBehind="userReport2.aspx.cs" Inherits="PMToolWeb.UserReport" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v12.2.Web, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
<div class="heading">
            <h2>
                UserReport
            </h2>
</div>
<table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblRoleName" runat="server" Text="Role:">
                        </dx:ASPxLabel>
                    </td>
                     <td>
                            <dx:ASPxComboBox ID="ddlrollname" runat="server">
                            <ValidationSettings Display="None" ErrorDisplayMode="None">
                            <RequiredField IsRequired="True" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="function(s, e) {e.isValid = s.GetValue() != '0'; if(e.isValid != true) { e.errorText = 'Required';}}" />
                            </dx:ASPxComboBox>
                    </td>
                     <td>
                        <dx:ASPxButton ID="btnOk" runat="server" Text="OK">
                        </dx:ASPxButton>
                    </td>
                    </tr>
</table>
                   



</asp:Content>
