﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/default.Master" AutoEventWireup="true" CodeBehind="closed-tasks.aspx.cs" Inherits="PMToolWeb.closed_tasks" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
<div  class="closed-task">
 <div class="heading">
            <h2>
                Closed Tasks
            </h2>
        </div>
   <div class="maindiv">
    <div class="subdiv">
     <div class="task-list-grid">



      <dx:aspxgridview ID="gvClosedTaskList" ClientInstanceName="gvClosedTaskList" 
             runat="server" KeyFieldName="TaskId"
                        AutoGenerateColumns="False" Width="777px" 
             onhtmlrowcreated="gvClosedTaskList_HtmlRowCreated">
                        <Columns>
                            <dx:GridViewDataColumn Caption="View" VisibleIndex="0" Width="25px">
                                <DataItemTemplate>
                                    <a href='task-detail.aspx?projectid=<%# Eval("ProjectId") %>&taskid=<%# Eval("TaskId") %>&status=Closed'>
                                        <img id="imgView" runat="server" src="~/images/view.png" alt="View" style="border: 0px;"
                                            title="View" /></a>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Last Name" FieldName="LastName" Width="65px" VisibleIndex="1"/>
                            <dx:GridViewDataColumn Caption="First Name" FieldName="FirstName" Width="65px" VisibleIndex="2"/>
                            <dx:GridViewDataColumn Caption="Project Name" Width="105px" FieldName="ProjectName" VisibleIndex="3">
                                <DataItemTemplate>
                                    <a href='task-detail.aspx?projectid=<%# Eval("ProjectId") %>&status=Closed' class="project-name">
                                        <%# Eval("ProjectName")%></a>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                           <dx:GridViewDataColumn Caption="Status" FieldName="TaskStatus" Width="45px" VisibleIndex="4">
                            <DataItemTemplate>
                            <%# Enum.Parse(typeof(PMToolWeb.TaskStatus), Convert.ToString(Eval("TaskStatus")))%>
                            </DataItemTemplate>
                           </dx:GridViewDataColumn> 
                            <dx:GridViewDataColumn Caption="Due In" FieldName="DueIn" Width="45px" VisibleIndex="5"/>
                            <dx:GridViewDataColumn Caption="Assigned By" FieldName="AssignByName" Width="100px" VisibleIndex="6"/>
                            <dx:GridViewDataColumn Caption="Days Open" FieldName="DaysOpen" Width="45px" VisibleIndex="7"/>
                            <dx:GridViewDataColumn Caption="Task ID" FieldName="TaskId" Width="45px" VisibleIndex="8">
                            
                                <DataItemTemplate>
                                     <a href='task-detail.aspx?projectid=<%# Eval("ProjectId") %>&taskid=<%# Eval("TaskId") %>&status=Closed' class="project-name">
                                    <%# Eval("TaskID")%></a>
                            </DataItemTemplate>
                            </dx:GridViewDataColumn>
                              
                        </Columns>
                        <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                    </dx:aspxgridview>




     </div> 
    </div> 
   </div> 
</div>
</asp:Content>
