﻿<%@ Page Title="Create New Task | Project Management Tool" Language="C#" MasterPageFile="~/masterpages/default.Master"
    AutoEventWireup="true" CodeBehind="create-task.aspx.cs" Inherits="PMToolWeb.create_task" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="scripts/audio-player.js"></script>
    <script type="text/javascript">
    // <![CDATA[
        function OnSelectedIndexChanged(ddlProjectName) {
            ddlModule.PerformCallback(ddlProjectName.GetValue().toString());
        }
        function OnSelectedIndexChanged1(ddlModule) {
            ddlComp.PerformCallback(ddlModule.GetValue().toString());
        }
        function bye() {
            window.close();
        }
        // ]]> 

        AudioPlayer.setup("scripts/player.swf", {
            width: 258,
            initialvolume: 100,
            transparentpagebg: "yes",
            left: "000000",
            lefticon: "FFFFFF"
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
    <div class="task-creator">
        <div class="heading">
            <h2>
                Create Task
            </h2>
        </div>
        <div class="maindiv">
            <div class="subdiv">
                <div class="div-message default-padding-left">
                    <asp:Label ID="lblStatus" runat="server" EnableViewState="false"></asp:Label>
                </div>
                <table cellpadding="2" cellspacing="3" border="0" width="100%">
                    <tr>
                        <td align="right" valign="top" width="30%">
                            &nbsp;
                        </td>
                        <td class="mandatory" colspan="2" width="65%">
                            <span class="red-color">*</span>&nbsp;Indicates Mandatory Response
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            Task Number:
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxLabel ID="lblTaskNumber" runat="server" Text="0" CssClass="auto-number">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <span class="red-color">*</span>Select Project Name:
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxComboBox ID="ddlProjectName" runat="server" DropDownStyle="DropDownList"
                                IncrementalFilteringMode="StartsWith" EnableSynchronization="False" Width="300px" OnSelectedIndexChanged="ddlProjectName_SelectedIndexChanged">
                                <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                </ValidationSettings>
                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnSelectedIndexChanged(s); }"
                                    Validation="function(s, e) {e.isValid = s.GetValue() != '0'; if(e.isValid != true) { e.errorText = 'Required';}}" />
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <span class="red-color">*</span>Select Module:
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxComboBox ID="ddlModule" runat="server" ClientInstanceName="ddlModule" OnCallback="ddlModule_Callback"
                                Width="300px">
                                <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                </ValidationSettings>
                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnSelectedIndexChanged1(s); }"
                                 Validation="function(s, e) {e.isValid = false; if(s.GetValue() != null && s.GetValue() != 0) {e.isValid = true;}; if(e.isValid != true) { e.errorText = 'Required';}}" />
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                     <tr runat="server" visible="false">
                        <td align="right" valign="top">
                          Component Number:
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxTextBox ID="txtComponentNumber" runat="server" CssClass="input-txt" Width="100px">
                          
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                     <tr>
                        <td align="right" valign="top">
                       Component :
                        </td>
                        <td align="left" valign="top">
                               <dx:ASPxComboBox ID="ddlComponent" ClientInstanceName="ddlComp"  runat="server" Width="300px" OnCallback="ddlComponent_Callback">
                                <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                </ValidationSettings>
                                <ClientSideEvents Validation="function(s, e) {e.isValid = false; if(s.GetValue() != 0) {e.isValid = true;}; if(e.isValid != true) { e.errorText = 'Required';}}" />
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            Wireframe Number:
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxTextBox ID="txtWireFrameNo" runat="server" CssClass="input-txt" Width="150px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <span class="red-color">*</span>Task Title:
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxTextBox ID="txtTaskTitle" runat="server" CssClass="input-txt" Width="425px">
                                <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="True" ErrorText="Required" />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            URL Address:
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxTextBox ID="txtUrl" runat="server" CssClass="input-txt" Width="425px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <span class="red-color">*</span>Task Description:
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxMemo ID="txtTaskDescription" runat="server" Height="175px" Width="425px" OnTextChanged="txtTaskDescription_TextChanged">
                            </dx:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            <span class="smalldesc">Describe the task in detail. What is the 'task' requirement?
                                What event must occur to prove the task is actually complete? How will completion
                                be measured? </span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <span class="red-color">*</span>Task Complition Status:
                        </td>
                        <td align="left" valign="middle">
                            <div class="status-images" id="divDetail" runat="server">
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" valign="middle">
                                            <asp:ImageButton ID="imgbtnStatus1" CssClass="toolTip" runat="server" ImageUrl="~/images/dotempty.jpg"
                                                CommandArgument="10" OnClick="imgbtnStatus_Click" Width="24px" Height="24px"
                                                ToolTip="Brain Storming" />
                                        </td>
                                        <td align="left" valign="middle">
                                            <asp:ImageButton ID="imgbtnStatus2" CssClass="toolTip" runat="server" ImageUrl="~/images/dotempty.jpg"
                                                CommandArgument="20" OnClick="imgbtnStatus_Click" ToolTip="Discussion" />
                                        </td>
                                        <td align="left" valign="middle">
                                            <asp:ImageButton ID="imgbtnStatus3" CssClass="toolTip" runat="server" ImageUrl="~/images/dotempty.jpg"
                                                CommandArgument="30" OnClick="imgbtnStatus_Click" ToolTip=" Debate" />
                                        </td>
                                        <td align="left" valign="middle">
                                            <asp:ImageButton ID="imgbtnStatus4" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                CommandArgument="40" OnClick="imgbtnStatus_Click" ToolTip=" Scoping" />
                                        </td>
                                        <td align="left" valign="middle">
                                            <asp:ImageButton ID="imgbtnStatus5" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                CommandArgument="50" OnClick="imgbtnStatus_Click" ToolTip="Programming" />
                                        </td>
                                        <td align="left" valign="middle">
                                            <asp:ImageButton ID="imgbtnStatus6" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                CommandArgument="60" OnClick="imgbtnStatus_Click" ToolTip=" Testing" />
                                        </td>
                                        <td align="left" valign="middle">
                                            <asp:ImageButton ID="imgbtnStatus7" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                CommandArgument="70" OnClick="imgbtnStatus_Click" ToolTip="Bug Found" />
                                        </td>
                                        <td align="left" valign="middle">
                                            <asp:ImageButton ID="imgbtnStatus8" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                CommandArgument="80" OnClick="imgbtnStatus_Click" ToolTip="Bug Fixes" />
                                        </td>
                                        <td align="left" valign="middle">
                                            <asp:ImageButton ID="imgbtnStatus9" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                CommandArgument="90" OnClick="imgbtnStatus_Click" ToolTip="Awaiting Client Approval" />
                                        </td>
                                        <td align="left" valign="middle">
                                            <asp:ImageButton ID="imgbtnStatus10" runat="server" CssClass="toolTip" ImageUrl="~/images/dotempty.jpg"
                                                CommandArgument="100" OnClick="imgbtnStatus_Click" ToolTip="Closed" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <span class="red-color">*</span>Select User:
                        </td>
                        <td align="left" valign="top">
                            <asp:CheckBoxList ID="chkUserList" runat="server">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <span class="red-color">*</span>Hours to Complete Task:
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxTextBox ID="txtHours" runat="server" CssClass="input-txt" Width="100px">
                                <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="True" ErrorText="Required" />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            <span class="smalldesc">You may enter a value in decimal format. For example, if the
                                needed time to complete the task is 15 minutes, then you would enter .25. If you
                                need 60 minutes, you would enter 1.0. If you need 90 minutes, you enter 1.5, etc
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <span class="red-color">*</span>Who is the Task Owner?
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxComboBox ID="ddlOwner" runat="server" Width="225px">
                                <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                </ValidationSettings>
                                <ClientSideEvents Validation="function(s, e) {e.isValid = s.GetValue() != '0'; if(e.isValid != true) { e.errorText = 'Required';}}" />
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                   
                      
                    <tr>
                        <td align="right" valign="top">
                            <span class="red-color">*</span>Task Due Date?
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxDateEdit ID="dtDueDate" runat="server" Width="100px">
                                <ValidationSettings Display="Dynamic" CausesValidation="false" ErrorDisplayMode="ImageWithTooltip"
                                    SetFocusOnError="True" ValidateOnLeave="False">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                                <%-- <ClientSideEvents Validation="function(s, e) {e.isValid = s.GetValue() != '0'; if(e.isValid != true) { e.errorText = 'Required';}}" />--%>
                            </dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            <div class="upload">
                                <table cellpadding="2" cellspacing="3" border="0">
                                    <tr>
                                        <td align="left" valign="top" colspan="2">
                                            <span class="smalldesc">Do you have an image to include as part of this task? If so,
                                                you can upload the image here so the Task Owner can view. If you have more than
                                                one image, you can upload after adding the first image. </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <dx:ASPxUploadControl ID="fuTaskImage" runat="server" Width="300px" Size="50">
                                            </dx:ASPxUploadControl>
                                        </td>
                                        <td align="left" valign="top">
                                            <dx:ASPxButton ID="btnUpload" runat="server" Text="Upload" CausesValidation="false"
                                                OnClick="btnUpload_Click">
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" colspan="2">
                                            <dx:ASPxDataView ID="dvTaskImages" runat="server" RowPerPage="3" OnItemCommand="dvTaskImages_ItemCommand"
                                                ItemStyle-Paddings-Padding="0px" ItemSpacing="10px" EmptyDataText="[No file uploaded]"
                                                ColumnCount="3">
                                                <ItemTemplate>
                                                    <div class="item-image">
                                                        <dx:ASPxImage ID="imgTaskImage" runat="server" ImageUrl='<%# PMToolWeb.Helper.GetTaskImage(this.TaskID, Convert.ToString(Eval("ImageName"))) %>'
                                                            Width="127" Height="127" />
                                                    </div>
                                                    <table cellpadding="2" cellspacing="3" width="100%">
                                                        <tr>
                                                            <td align="left" valign="middle">
                                                                <asp:LinkButton ID="lnkbtnTaskImageDelete" runat="server" CommandName="Delete" CommandArgument='<%# Eval("TaskImagesId") %>'
                                                                    OnClientClick="return confirm('Are you sure want delete this task image?');"
                                                                    CausesValidation="false" CssClass="deletebutton">Delete</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                <PagerSettings>
                                                    <AllButton Visible="True">
                                                    </AllButton>
                                                </PagerSettings>
                                            </dx:ASPxDataView>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            <div class="upload">
                                <table cellpadding="2" cellspacing="3" border="0">
                                    <tr>
                                        <td align="left" valign="top" colspan="2">
                                            <span class="smalldesc">Do you have an audio to include as part of this task? If so,
                                                you can upload the audio here so the Task Owner can listen.</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <dx:ASPxUploadControl ID="fileuploadAudio" runat="server" Width="300px" Size="50">
                                            </dx:ASPxUploadControl>
                                        </td>
                                        <td align="left" valign="top">
                                            <dx:ASPxButton ID="btnUploadAudio" runat="server" Text="Upload" CausesValidation="false"
                                                OnClick="btnUploadAudio_Click"></dx:ASPxButton> 
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <div id="divaudioPlayer" runat="server" visible="true">
                                                <asp:Label ID="lblAudioplayer" runat="server"></asp:Label><br />
                                                <br />
                                                <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" OnClientClick="return confirm('Are you sure want delete this audio file?');">Delete</asp:LinkButton>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
        
                    <tr>
                        <td align="right" valign="top">
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            <dx:ASPxButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
