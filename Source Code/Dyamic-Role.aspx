﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" bgcolor="purple">
    <div>
    
        <table style="width:100%;">
            <tr>
                <td bgcolor="White">
                    Role management helps you manage authorization, which enables you to specify the 
                    resources that users in your application are allowed to access. Role management 
                    lets you treat groups of users as a unit by assigning users to roles such as 
                    manager, sales, member, and so on. (In Windows, you create roles by assigning 
                    users to groups such as Administrators, Power Users, and so on.)</td>
            </tr>
            <tr>
                <td>
                    After you have established roles, you can create access rules in your 
                    application. For example, your site might include a set of pages that you want 
                    to display only to members. Similarly, you might want to show or hide a part of 
                    a page based on whether the current user is a manager. By using roles, you can 
                    establish these types of rules independent from individual application users. 
                    For example, you do not have to grant individual members of your site access to 
                    member-only pages. Instead, you can grant access to the role of member and then 
                    just add and remove users from that role as people sign up or let their 
                    memberships lapse</td>
            </tr>
            <tr>
                <td>
                    Users can belong to more than one role. For example, if your site is a 
                    discussion forum, some users might be in the roles of both member and moderator. 
                    You might define each role to have different rights on the site, and a user who 
                    is in both roles would then have both sets of rights.</td>
            </tr>
            <tr>
                <td bgcolor="White">
                    Even if your application has only a few users, you might still find it 
                    convenient to create roles. Roles give you flexibility to change permissions and 
                    add and remove users without having to make changes throughout the site. As you 
                    define more access rules for your application, roles become a more convenient 
                    way to apply the changes to groups of users.</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
