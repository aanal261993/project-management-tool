﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/default.Master" AutoEventWireup="true" CodeBehind="UpdatePassword.aspx.cs" Inherits="PMToolWeb.UpdatePassword" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
    <div class="heading">
            <h2>
               Edit Profile
            </h2>
        </div>
    <div class="user-edit">
            <table width="100%">
                <tr>
                    <td width="50%" valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2">
                                    </td>
                                <td class="blueline">
                                </td>
                            </tr>
                             <tr>
                                <td class="username">
                                    <span>*Old Password : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="txtoldpwd" runat="server" CssClass="txtbox" 
                                        MaxLength="50" Theme="Default">
                                       </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                    <span>*New Password : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="txtpass1" runat="server" CssClass="txtbox" MaxLength="50">
                                        <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                            <RegularExpression ErrorText="Invalid firstName" ValidationExpression="[a-z A-Z]*" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                    <span>*Re-Password : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="txtpass2" runat="server" CssClass="txtbox" MaxLength="50">
                                        <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                            <RegularExpression ErrorText="Invalid lastName" ValidationExpression="[a-z A-Z]*" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                                </tr>
                                <tr>
                                <td>
                                <dx:ASPxButton ID="btnUpdate" runat="server" Text="Update" CausesValidation="true" OnClick="btnUpdate_Click"
                                                    ToolTip="Update" Height="25px">
                                                </dx:ASPxButton>
                            </td>
                            </tr>
                            </table>
       </td>                     
       </tr>
       </table>
     
       </div>
</asp:Content>
