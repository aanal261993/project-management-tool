﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Blue Eon Solutions</title>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-17304516-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <link rel="Stylesheet" href="Default.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" border="0" style="color: #666699; width: 700px;
            border: px inset black; text-align: center; background: white; margin-left: auto;
            margin-right: auto;">
            <!-- Page header -->
            <tr>
                <td style="text-align: left">
                    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                        <tr>
                            <td style="text-align: left; vertical-align: top;">
                                <asp:HyperLink ID="lnkLogo" runat="server" ImageUrl="~/App_Themes/CurrentTheme/Images/Logo.jpg"
                                    NavigateUrl="~/" ToolTip="Mxicoders Home Page" />
                            </td>
                            <td style="width: 50%; text-align: right; vertical-align: top;">
                                <asp:HyperLink ID="lnkHome" runat="server" NavigateUrl="~/" Text="HOME" />
                                &nbsp; &nbsp; &nbsp;
                                <asp:HyperLink ID="lnkContact" runat="server" NavigateUrl="~/Company_ContactUs.aspx"
                                    Text="CONTACT" />
                                &nbsp; &nbsp; &nbsp;
                                <asp:HyperLink ID="lnkSiteMap" runat="server" NavigateUrl="~/Company_SiteMap.aspx"
                                    Text="SITEMAP" />
                                &nbsp; &nbsp; &nbsp; 1.866.317.0256
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Separator -->
            <tr style="height: 2px">
                <td style="background-image: url(App_Themes/CurrentTheme/Images/Borders/CenterBorder_Gray.jpg);
                    background-repeat: repeat;">
                </td>
            </tr>
            <tr style="height: 5px">
                <td>
                </td>
            </tr>
            <!-- Content holder -->
            <tr>
                <td style="text-align: left">
                    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                        <tr>
                            <td style="text-align: left; vertical-align: top; width: 25%">
                                <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                                    <tr>
                                        <td style="width: 25px">
                                            <asp:Image ID="imgBullet_Products" runat="server" ImageUrl="~/App_Themes/CurrentTheme/Images/Bullet.jpg" />
                                        </td>
                                        <td style="vertical-align: middle">
                                            <span class="AltFontColor">PRODUCTS </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink1" runat="server" NavigateUrl="~/CityConnect.aspx" Text="City Connect" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink2" runat="server" NavigateUrl="~/WireAtlas.aspx" Text="Wire Atlas" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink3" runat="server" NavigateUrl="~/GoSimon.aspx" Text="Go Simon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink4" runat="server" NavigateUrl="~/RealView.aspx" Text="Real View " />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Themes/CurrentTheme/Images/Bullet.jpg" />
                                        </td>
                                        <td style="vertical-align: middle">
                                            <span class="AltFontColor">SERVICES </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink5" runat="server" NavigateUrl="~/Services_ApplicationDevelopment.aspx"
                                                Text="Application Development" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px">
                                            &nbsp;
                                        </td>
                                        <td style="height: 19px">
                                            <asp:HyperLink ID="Hyperlink6" runat="server" NavigateUrl="~/Services_ScopeDevelopment.aspx"
                                                Text="Scope Development" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink7" runat="server" NavigateUrl="~/Services_ApplicationDesign.aspx"
                                                Text="Application Design" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px">
                                            &nbsp;
                                        </td>
                                        <td style="height: 19px">
                                            <asp:HyperLink ID="Hyperlink8" runat="server" NavigateUrl="~/Services_Wireframes.aspx"
                                                Text="Wireframing" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink9" runat="server" NavigateUrl="~/Services_Programming.aspx"
                                                Text="Programming" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink10" runat="server" NavigateUrl="~/Services_ApplicationTesting.aspx"
                                                Text="Application Testing" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink11" runat="server" NavigateUrl="~/Services_Prototyping.aspx"
                                                Text="Prototyping" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink12" runat="server" NavigateUrl="~/Services_Training.aspx"
                                                Text="Training Materials" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink13" runat="server" NavigateUrl="~/Services_Consulting.aspx"
                                                Text="Consulting" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/App_Themes/CurrentTheme/Images/Bullet.jpg" />
                                        </td>
                                        <td style="vertical-align: middle">
                                            <span class="AltFontColor">COMPANY </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink14" runat="server" NavigateUrl="~/Company_AboutMxi.aspx"
                                                Text="About Mxicoders " />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink20" runat="server" NavigateUrl="~/Company_Projects.aspx"
                                                Text="Projects" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink15" runat="server" NavigateUrl="~/Company_Values.aspx"
                                                Text="Values" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink19" runat="server" NavigateUrl="~/Company_Press.aspx"
                                                Text="Press" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink16" runat="server" NavigateUrl="~/Company_ContactUs.aspx"
                                                Text="Contact Us" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink17" runat="server" NavigateUrl="~/Company_TermsOfUse.aspx"
                                                Text="Terms of Use" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="Hyperlink18" runat="server" NavigateUrl="~/Company_Privacy.aspx"
                                                Text="Privacy" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <iframe src="MxiLoginPage.aspx" marginheight="0" marginwidth="0" frameborder="0"
                                                scrolling="no" width="200" height="350"></iframe>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="background-image: url(App_Themes/CurrentTheme/Images/Borders/VerticalBar_Gray.jpg);
                                background-repeat: repeat-y">
                                &nbsp;
                            </td>
                            <td style="text-align: left; vertical-align: top;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Separator -->
            <tr style="height: 5px">
                <td>
                </td>
            </tr>
            <tr style="height: 2px">
                <td style="background-image: url(App_Themes/CurrentTheme/Images/Borders/CenterBorder_Gray.jpg);
                    background-repeat: repeat;">
                </td>
            </tr>
            <tr style="height: 5px">
                <td>
                </td>
            </tr>
            <!-- Page footer -->
            <tr>
                <td style="text-align: left">
                    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                        <tr>
                            <td style="text-align: left; vertical-align: top;">
                                Mxicoders Software Solution<br />
                                910, Akshat Tower,<br />
                                Opp.Pakwan Restaurant, S.G.Highway,<br />
                                Bodakdev, Ahmedabad - 380054<br />
                                Web: <a href="http://www.mxicoders.com">www.mxicoders.com</a>
                            </td>
                            <td style="width: 50%; text-align: right; vertical-align: top;">
                                Copyright © 2013- All Rights Reserved<br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
