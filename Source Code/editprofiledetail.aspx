﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/default.Master" AutoEventWireup="true" CodeBehind="editprofiledetail.aspx.cs" Inherits="PMToolWeb.editprofiledetail" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
 <div class="edit-profile">
        <div class="heading">
            <h2>
               Edit Profile
            </h2>
        </div>
        <div class="div-message">
            <asp:Label ID="lblStatus" runat="server" EnableViewState="false"></asp:Label>
            
        </div>
     
         <div class="user-edit">
            <table width="100%">
                <tr>
                    <td width="50%" valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2">
                                    </td>
                                <td class="blueline">
                                </td>
                            </tr>
                             <tr>
                                <td class="username">
                                    <span>*User Name : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" CssClass="txtbox" 
                                        MaxLength="50" Theme="Default">
                                       </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                    <span>*First Name : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="txtFirstName" runat="server" CssClass="txtbox" MaxLength="50">
                                        <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                            <RegularExpression ErrorText="Invalid firstName" ValidationExpression="[a-z A-Z]*" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                    <span>*Last Name : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="txtLastName" runat="server" CssClass="txtbox" MaxLength="50">
                                        <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                            <RegularExpression ErrorText="Invalid lastName" ValidationExpression="[a-z A-Z]*" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                                </tr>
                                <tr>
                                <td>
                                <dx:ASPxButton ID="btnUpdate" runat="server" Text="Update" CausesValidation="true" OnClick="btnUpdate_Click"
                                                    ToolTip="Update" Height="25px">
                                                </dx:ASPxButton>
                            </td>
                            </tr>
       </td>                     
       </tr>
       </table>
       </table>
       </div>
      
</asp:Content>
