﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/client.Master" AutoEventWireup="true"
    CodeBehind="client_default.aspx.cs" Inherits="PMToolWeb.client_default" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register TagPrefix="uc" TagName="clientHeader" Src="~/controls/clientHeaderControl.ascx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="mainTable" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
        <td class="headerLinks">
        <a href="#">Home</a>  &nbsp;|  &nbsp;
        <a href="#">Open New Task</a>  &nbsp;|  &nbsp;
        <a href="#">View Closed Tasks</a>
        </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="breadcumb">
                    <tr>
                        <td>
                            <b>Active Tasks<dx:ASPxLabel ID="lblTotalTask" runat="server" Text="">
                            </dx:ASPxLabel>
                            </b>
                        </td>
                        <td width="4" class="none">
                            <img src="images/spacer.gif" width="4" height="1" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
             <img src="images/spacer.gif" width="4" height="1" alt="" />
                <dx:ASPxGridView Width="100%" ID="gvActiveTask" runat="server">
                    <Columns>
                        <dx:GridViewDataColumn Caption="View" VisibleIndex="0" Width="15px">
                            <DataItemTemplate>
                                <a href="#">
                                    <img id="imgView" runat="server" src="~/images/view.png" alt="View" style="border: 0px;"
                                        title="View" /></a>
                            </DataItemTemplate>
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Open Date" FieldName="Createddate" Width="55px" VisibleIndex="1" />
                        <dx:GridViewDataColumn Caption="Module Name" FieldName="ModuleName" Width="150px"
                            VisibleIndex="2" />
                        <dx:GridViewDataColumn Caption="Task Number" CellStyle-HorizontalAlign="Left" Width="40px"
                            FieldName="TaskId" VisibleIndex="3">
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Subject" FieldName="Title" Width="150px" VisibleIndex="5" />
                        <dx:GridViewDataColumn Caption="Updates" FieldName="update" Width="25px" VisibleIndex="6" />
                    </Columns>
                    <SettingsPager PageSize="10" Position="Bottom" SEOFriendly="Disabled" AlwaysShowPager="True">
                        <Summary AllPagesText="omotional events" Text="Page {0} of {1} ({2} Tasks)" />
                    </SettingsPager>
                </dx:ASPxGridView>
            </td>
        </tr>
    </table>
</asp:Content>
