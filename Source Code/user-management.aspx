﻿<%@ Page Title="User Management | Project Management Tool" Language="C#" MasterPageFile="~/masterpages/default.Master"
    AutoEventWireup="true" CodeBehind="user-management.aspx.cs" Inherits="PMToolWeb.user_management" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphAdmin" runat="server">
    <div class="user-management">
        <div class="heading">
            <h2>
                User Management
            </h2>
        </div>
        <div class="div-message">
            <asp:Label ID="lblStatus" runat="server" EnableViewState="false"></asp:Label>
        </div>
        <div class="sub-heading">
       
        <div class="users">
            <dx:ASPxGridView ID="gvUsers" ClientInstanceName="grid" runat="server" KeyFieldName="UserId"
                Width="772px" AutoGenerateColumns="False" OnRowCommand="gvUsers_RowCommand" OnHtmlRowCreated="gvUsers_HtmlRowCreated">
                <Columns>
                    <dx:GridViewDataColumn Caption="View" VisibleIndex="0" Width="50">
                        <DataItemTemplate>
                            <asp:LinkButton ID="lnkView" runat="server" CommandName="Edit" CausesValidation="false"
                                CommandArgument='<%# Eval("UserId") %>'>
                                <img id="Img1" runat="server" src="~/images/img_Search.png" alt="View" style="border: 0px;"
                                    title="View" /></asp:LinkButton>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataCheckColumn Caption="Status" FieldName="IsActive" VisibleIndex="1"
                        Width="50" />
                    <dx:GridViewDataTextColumn Caption="First Name" FieldName="FirstName" VisibleIndex="2"
                        Width="100" />
                    <dx:GridViewDataColumn Caption="Last Name" FieldName="LastName" VisibleIndex="3"
                        Width="100" />
                    <dx:GridViewDataColumn Caption="Role" FieldName="RoleName" VisibleIndex="4" Width="170" />
                    <dx:GridViewDataColumn Caption="RoleId" FieldName="RoleId" VisibleIndex="5" Visible="false" />
                    <dx:GridViewDataColumn Caption="Open Tasks" FieldName="OpenTask" VisibleIndex="6"
                        Visible="true" />
                    <dx:GridViewDataColumn Caption="Closed Tasks" FieldName="CloseTask" VisibleIndex="7"
                        Visible="true" />
                    <dx:GridViewDataColumn Caption="Last Login" VisibleIndex="8">
                        <DataItemTemplate>
                            <dx:ASPxLabel ID="lblLastLogin" runat="server" Text='<%# GetLastLogin(Convert.ToString(Eval("LastLogin")),Convert.ToInt64(Eval("UserId"))) %>'>
                            </dx:ASPxLabel>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>
                <SettingsBehavior ColumnResizeMode="NextColumn" />
                <SettingsPager PageSize="10" Position="Bottom" SEOFriendly="Disabled">
                    <FirstPageButton Visible="True">
                    </FirstPageButton>
                    <LastPageButton Visible="True">
                    </LastPageButton>
                    <Summary AllPagesText="Pages: {0} - {1} ({2} P)" Text="Page {0} of {1} ({2} Users)" />
                </SettingsPager>
                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
            </dx:ASPxGridView>
        </div>
        <div class="sub-heading2">
            <h2>
                User Manager</h2>
        </div>
        <div class="user-edit">
            <table width="100%">
                <tr>
                    <td width="50%" valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2">
                                    <span>*Indicates a mandatory response</span>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                    <span>*First Name : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="txtFirstName" runat="server" CssClass="txtbox" MaxLength="50">
                                        <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                            <RegularExpression ErrorText="Invalid firstName" ValidationExpression="[a-z A-Z]*" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                    <span>*Last Name : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="txtLastName" runat="server" CssClass="txtbox" MaxLength="50">
                                        <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                            <RegularExpression ErrorText="Invalid lastName" ValidationExpression="[a-z A-Z]*" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                    <span>*User Name : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="txtUserName" runat="server" CssClass="txtbox" ClientInstanceName="username"
                                        MaxLength="150" EnableClientSideAPI="true">
                                        <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            <RegularExpression ErrorText="Invalid userName" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                    <span>*Re-enter User Name : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="txtReEnterUserName" runat="server" CssClass="txtbox" MaxLength="150">
                                        <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            <RegularExpression ErrorText="Invalid confirm username" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                        </ValidationSettings>
                                        <ClientSideEvents Validation="function(s, e) {
	                            var originalPasswd = username.GetText();
                                var currentPasswd = s.GetText();
                                e.isValid = (originalPasswd  == currentPasswd );
                                if(e.isValid != true) { e.errorText = 'Username mismatch';}
                            }" />
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                    <span>*Password : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="txtPassword" runat="server" CssClass="txtbox" ClientInstanceName="password"
                                        MaxLength="50" EnableClientSideAPI="true">
                                        <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                    <span>*Re-enter Password : </span>
                                </td>
                                <td class="txtfirstname">
                                    <dx:ASPxTextBox ID="txtReenterPassword" runat="server" CssClass="txtbox" MaxLength="50">
                                        <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                        </ValidationSettings>
                                        <ClientSideEvents Validation="function(s, e) {
	                            var originalPasswd = password.GetText();
                                var currentPasswd = s.GetText();
                                e.isValid = (originalPasswd  == currentPasswd );
                                if(e.isValid != true) { e.errorText = 'Password mismatch';}
                            }" />
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                </td>
                                <td class="txtfirstname">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <span>*Role : </span>
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="ddlRole" runat="server" Width="100px" 
                                                    onselectedindexchanged="ddlRole_SelectedIndexChanged" AutoPostBack="true">
                                                    <ValidationSettings SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                                    </ValidationSettings>
                                                    <ClientSideEvents Validation="function(s, e) {
                                              e.isValid = s.GetValue() != '-Select-';
                            }" />
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td class="firstname">
                                </td>
                                <td class="txtfirstname">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <span>Status : </span>
                                            </td>
                                            <td>
                                                <dx:ASPxCheckBox ID="chkStatus" runat="server" Checked="True">
                                                </dx:ASPxCheckBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="blueline">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table cellpadding="2" cellspacing="3" class="btn">
                                        <tr>
                                            <td>
                                                <dx:ASPxButton ID="btnSave" runat="server" Text="Save" CausesValidation="true" OnClick="btnSave_Click"
                                                    ToolTip="Save">
                                                </dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete User" CausesValidation="false"
                                                    Enabled="false" OnClick="btnDeleteUser_Click">
                                                    <ClientSideEvents Click="function(s, e){ 
                                                            e.processOnServer = confirm('Are you sure want to remove this User?');
                                                          }" />
                                                </dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false"
                                                    ToolTip="Cancel" OnClick="btnCancel_Click">
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%" valign="top">
                        <table>
                            <tr>
                                <td>
                                    <b>Project Viewing Rights</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    This User may access and view the following projects.
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 25px;">
                                    <asp:CheckBoxList ID="chkProjects" runat="server">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
